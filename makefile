#    xpr - A rewrite of the SUS core programs.
#    Copyright (C) 2022, 2023 Ethan Masse
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

CC = gcc
CFLAGS = -std=c17 -Wall -Wextra -Wpedantic
OBJDIR = build
BINDIR = bin
SRCDIR = src
SHAREDDIR = src/shared
SOURCEFILES = $(SRCDIR)/basename.c \
	      $(SRCDIR)/cat.c \
	      $(SRCDIR)/cmp.c \
	      $(SRCDIR)/cp.c \
	      $(SRCDIR)/dirname.c \
	      $(SRCDIR)/echo.c \
	      $(SRCDIR)/false.c \
	      $(SRCDIR)/head.c \
	      $(SRCDIR)/rmdir.c \
	      $(SRCDIR)/tail.c \
	      $(SRCDIR)/tee.c \
	      $(SRCDIR)/touch.c \
	      $(SRCDIR)/true.c \
	      $(SRCDIR)/uniq.c \
	      $(SHAREDDIR)/utils.c \
	      $(SRCDIR)/wc.c

.PHONY: build test clean

build: $(SOURCEFILES) $(BINDIR)/
	$(CC) $(CFLAGS) -o $(BINDIR)/basename $(OBJDIR)/basename.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $(BINDIR)/cat $(OBJDIR)/cat.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $(BINDIR)/cmp $(OBJDIR)/cmp.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $(BINDIR)/cp $(OBJDIR)/cp.o
	$(CC) $(CFLAGS) -o $(BINDIR)/dirname $(OBJDIR)/dirname.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $(BINDIR)/echo $(OBJDIR)/echo.o
	$(CC) $(CFLAGS) -o $(BINDIR)/false $(OBJDIR)/false.o
	$(CC) $(CFLAGS) -o $(BINDIR)/head $(OBJDIR)/head.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $(BINDIR)/rmdir $(OBJDIR)/rmdir.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $(BINDIR)/tail $(OBJDIR)/tail.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $(BINDIR)/tee $(OBJDIR)/tee.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $(BINDIR)/touch $(OBJDIR)/touch.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $(BINDIR)/true $(OBJDIR)/true.o
	$(CC) $(CFLAGS) -o $(BINDIR)/uniq $(OBJDIR)/uniq.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $(BINDIR)/wc $(OBJDIR)/wc.o $(OBJDIR)/utils.o


$(SOURCEFILES): $(OBJDIR)/
	$(CC) $(CFLAGS) -I$(SRCDIR)/ -c -o $(OBJDIR)/$$(basename -- $@ .c).o $@

$(OBJDIR)/:
	mkdir -p $(OBJDIR)/

$(BINDIR)/:
	mkdir -p $(BINDIR)/

test:
	./testcases.sh

#We make absolutely certain we're not about to rm -rf /bin/,
#which, though unlikely, could happen if someone ran
#"sudo make clean" on this makefile while navigated to / .
clean:
	if [ x"$$(pwd)" != x"/" ]; then \
		if [ x"$$(pwd)" != x"//" ]; then \
			if [ x"$$(pwd)" != x"/usr/" ]; then \
				rm -rf $(BINDIR) $(OBJDIR); \
			fi \
		fi \
	fi

