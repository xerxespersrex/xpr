# xpr

A fun attempt to rewrite the Single Unix Specification core utilities from scratch. Programs are aimed at POSIX/SUS behavior and not extended GNU behavior currently (i.e., no "--" options, strict ordering of options, less options).

## License

All files in this repository are licensed under the GNU AGPLv3+ license (except for the text of the GNU AGPLv3 in the LICENSE file, which is distributable copyrighted material of the Free Software Foundation).

## Programs

Right now, included with xpr are:

* basename
* cat
* cmp
* cp (basic)
* dirname
* echo
* false
* head
* rmdir (basic)
* tail
* tee (missing -i flag)
* touch
* true
* uniq
* wc (missing -m flag)

## Dependencies

Depends on having a decently POSIX-compliant system (for example, one that uses glibc). Compiles using gcc with the accompanying scripts, but it is not a hard dependency; the target is standard C17 + POSIX extensions and libraries, and no GNU compiler/library extensions are, or will be, included (unless there's some absurdly good reason for it).

## Build/Install

Compile by cloning the git repository, cd-ing into the root of the git clone, and running "make" in your terminal. The compiled programs will be in the bin/ directory. There is an included testcases.sh script which tests the utilities against various known good outputs to see if they're functioning properly; this can be run either directly or with "make test".

Running "make clean" will delete the last build of the utilities, which involves running "rm -rf bin/". In very specific and rare situations this could be dangerous (i.e. make is run as root and while navigated to / or /usr/ or something). There's a very basic check to make sure you're not deleting /bin/, but some other crucial directories are not checked against. Please do not run the makefile as root. It cannot install to your path anyways, so there should be no need to.

I don't currently recommend installing any of these to your actual PATH because they're highly unfinished and are just demonstrations. If you want to try them out, use them by running them from an absolute/relative path, like "bin/touch -a -r reffile touchedfile".

Even if these did work perfectly, I still would not recommend installing them to your PATH as a replacement for their counterpart GNU programs if you're running a GNU or GNU/Linux system, because everything will break catastrophically. I have an idea to rename them to different names if desired with a macro in the source file, turning "touch" into "grasp" for example, but they're not finished enough for it to matter yet anyways.

## Merge requests

Currently not taking merge requests. The project is meant to be for my own learning purposes, and I'm also not very fluent with git or GitLab and I'm not entirely sure how to manage merge requests at all, let alone do so effectively. I am, however, not opposed to opening this project up once it's a lot further off the ground than it is now.

## Contact

Direct inquiries to: xerxes@persrex.xyz

## Donate

Donation information can be found here: https://persrex.xyz/info/donation.html

Please send an email alongside it with the amount and donation method telling me the donation was for xpr.

