//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2022 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _XOPEN_SOURCE 700

#include <stdbool.h>
#include <stdio.h>

#include "shared/utils.h"

int main(int argc, char *argv[]);
int printfromfile(char *const filename);
int printfromstdin();

int main(int argc, char *argv[])
{
	//Set to true if function has
	//non-option argument.
	bool hasargument = false;

	//State of looking for options
	//in arguments.
	bool lookingforoptions = true;

	//Flag to print from stdin instead
	//of file.
	bool standardinput = false;

	//Tracks if we had an error reading
	//a file/stdin to return a non-0
	//value from the utility.
	bool error = false;

	//Increment variables.
	int i = 1;
	int j = 1;

	//Main program logic.
	for (i = 1; i < argc; i++)
	{
		//If we're out of the options,
		//turn off lookingforoptions.
		if (argv[i][0] != '-')
		{
			lookingforoptions = false;
		}

		//If we get - as an argument, that
		//means read stdin.
		else if (stringcompare(argv[i], "-"))
		{
			lookingforoptions = false;

			standardinput = true;
		}

		//If we're looking for options
		//and we found the format of an
		//option, let's see what we got.
		else if (lookingforoptions)
		{
			//If we get the special option delimiter,
			//skip it and stop looking for options.
			if (stringcompare(argv[i], "--"))
			{
				lookingforoptions = false;

				continue;
			}

			//Option parser.
			for (j = 1; argv[i][j] != '\0'; ++j)
			{
				//-u: print output unbuffered.
				if (argv[i][j] == 'u')
				{
					//Do nothing.
					//We already print unbuffered
					//by default (which is POSIX
					//compliant).
				}

				else
				{
					fprintf(stderr, "cat: unknown option %c encountered in option string %s\n", argv[i][j], argv[i]);

					return 50;
				}
			}

			continue;
		}

		//We had an argument to the
		//program; flip this to true.
		hasargument = true;

		//If we get just "-" as an
		//argument, that means print
		//stdin (e.g. from pipe).
		if (standardinput)
		{
			standardinput = false;

			printfromstdin();

			continue;
		}

		//Otherwise print from the given
		//file.
		if (printfromfile(argv[i]) != 0)
		{
			error = true;
		}
	}

	//If we had no arguments, print stdin.
	if (!hasargument)
	{
		printfromstdin();
	}

	if (error)
	{
		return 1;
	}

	return 0;
}

//Function to print from file.
int printfromfile(char *const filename)
{
	//Pointer to file stream.
	FILE *fptr = NULL;

	//Current byte being read from stdin.
	int c = EOF;

	//Open the file.
	fptr = fopen(filename, "rb");

	//If reading failed,
	//return error code.
	if (fptr == NULL)
	{
		fprintf(stderr, "cat: could not open %s\n", filename);

		return 1;
	}

	//Print the contents of the file.
	for (;;)
	{
		c = fgetc(fptr);

		if (ferror(fptr))
		{
			fprintf(stderr, "cat: error reading file %s\n", filename);

			//Remember to close the file here!
			fclose(fptr);

			return 2;
		}

		if (c == EOF)
		{
			break;
		}

		printf("%c", (char)c);
	}

	//Close file.
	fclose(fptr);

	//Return success.
	return 0;
}

//Function to print from stdin.
int printfromstdin()
{
	//Current byte being read.
	int c = EOF;

	//We need clearerr(stdin) to
	//clear a lingering EOF from
	//the last read, if there's
	//multiple - arguments to
	//cat. (e.g. "cat - cat.c -").
	clearerr(stdin);

	for (;;)
	{
		c = getchar();

		if (c == EOF)
		{
			break;
		}

		printf("%c", (char)c);
	}

	return 0;
}

