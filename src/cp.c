//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2022 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _XOPEN_SOURCE 700

#include <stdio.h>

int main(int argc, char *argv[])
{
	FILE *foneptr = NULL;
	FILE *ftwoptr = NULL;

	int foneloc = 0;
	int ftwoloc = 0;

	int c = EOF;

	int i = 1;

	for (i = 1; i < (argc - 1); ++i)
	{
		if (argv[i][0] == '-')
		{
			continue;
		}

		foneptr = fopen(argv[i], "rb");

		if (foneptr == NULL)
		{
			fprintf(stderr, "cp: could not read input file %s, skipping\n", argv[i]);

			continue;
		}

		foneloc = i;

		break;
	}

	if (foneptr == NULL)
	{
		fprintf(stderr, "cp: no input file found\n");

		return 1;
	}

	if (i == (argc - 1))
	{
		fprintf(stderr, "cp: no output file found\n");

		fclose(foneptr);

		return 2;
	}

	for (i = i + 1; i < argc; ++i)
	{
		if (argv[i][0] == '-')
		{
			continue;
		}

		ftwoptr = fopen(argv[i], "wb");

		if (ftwoptr == NULL)
		{
			continue;
		}

		ftwoloc = i;

		break;
	}

	if (ftwoptr == NULL)
	{
		fprintf(stderr, "cp: no output file found\n");

		fclose(foneptr);

		return 3;
	}

	for (;;)
	{
		c = fgetc(foneptr);

		if (ferror(foneptr))
		{
			fprintf(stderr, "cp: error reading file %s\n", argv[foneloc]);

			fclose(foneptr);
			fclose(ftwoptr);

			return 10;
		}

		if (c == EOF)
		{
			break;
		}

		c = fputc(c, ftwoptr);

		if ((c < 0) || (c == EOF) || ferror(ftwoptr))
		{
			fprintf(stderr, "cp: error writing to file %s\n", argv[ftwoloc]);

			fclose(foneptr);
			fclose(ftwoptr);

			return 15;
		}
	}

	fclose(foneptr);
	fclose(ftwoptr);

	return 0;
}

