//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2022 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef UTILS_H
#define UTILS_H

#define _XOPEN_SOURCE 700

int chartonum(const char c);
int fastexponent(const int base, const int power);
bool iswhitespace(const char c);
bool stringcompare(char *const stringone, char *const stringtwo);
char* stringcopy(char *const input);
int stringtonum(char *const string);

#endif

