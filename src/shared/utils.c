//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2022 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _XOPEN_SOURCE 700

#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>

#include "utils.h"

//Convert a char to an int.
//Works for '0' - '9' in ASCII.
int chartonum(const char c)
{
        int converted = (int)(c - '0');

        if ((converted < 0) || (converted > 9))
        {
                return -89;
        }

        return converted;
}

//Quick and dirty exponent function,
//used for powers of 10 in stringtonum().
int fastexponent(const int base, const int power)
{
        int result = 1;

        int i = 1;

        for (i = 1; i <= power; ++i)
        {
                result *= base;
        }

        return result;
}

//Determines if a character is a
//whitespace character or not.
bool iswhitespace(const char c)
{
        if ((c == ' ') || (c == '\t') || (c == '\n')
            || (c == '\v') || (c == '\f') || (c == '\r'))
        {
                return true;
        }

        return false;
}

//Basic string compare function.
bool stringcompare(char *const stringone, char *const stringtwo)
{
        int i = 0;

        for (i = 0; (stringone[i] != '\0') && (stringtwo[i] != '\0'); ++i)
        {
                if (stringone[i] != stringtwo[i])
                {
                        return false;
                }
        }

        if (stringone[i] != stringtwo[i])
        {
                return false;
        }

        return true;
}

//Copies a character string into a new
//location in memory and returns a pointer
//to it.
char* stringcopy(char *const input)
{
	int i = 0;

	char* output = NULL;

	for (i = 0; input[i] != '\0'; ++i)
	{
		//Do nothing.
	}

	++i;

	output = (char*)calloc(i, sizeof(char));

	for (i = 0; input[i] != '\0'; ++i)
	{
		output[i] = input[i];
	}

	output[i] = input[i];

	return output;
}

//Converts a string to an integer.
//Does not handle negatives.
int stringtonum(char *const string)
{
	//Digits of input string.
	int digits = -1;

	//Whether or not we have
	//the potential to overflow
	//an integer.
	bool spooky = false;

	//Stores result to return.
	int result = 0;

	//Temp variables for calculation
	int tempcheck = -15;
	int tempdigit = -1;
	int temptensplace = 1;

	//Increment variable.
	int i = 0;

	//Count digits in input.
	for (i = 0; string[i] != '\0'; ++i)
	{
		//Do nothing.
	}

	digits = i;

	//If over 10 digits, we're definitely
	//overflowing an int.
	if (digits > 10)
	{
		return -1;
	}

	//If exactly 10 digits, we're spooked.
	if (digits == 10)
	{
		spooky = true;
	}

	//If less than 1 digit, error.
	if (digits < 1)
	{
		return -3;
	}

	//Conversion logic.
	for (i = 0; string[i] != '\0'; ++i)
	{
		//Get current digit.
		tempdigit = chartonum(string[i]);

		//If converting to a digit failed, error.
		if ((tempdigit < 0) || (tempdigit > 9))
		{
			return -10;
		}

		//Store the 10s place as an int for
		//later calculations.
		temptensplace = fastexponent(10, (digits - 1));

		//If we're spooked...
		if (spooky)
		{
			//Get the digit of INT_MAX at our current
			//10s place.
			tempcheck = (INT_MAX / temptensplace) % 10;

			//If it's higher, we're good.
			if (tempcheck > tempdigit)
			{
				spooky = false;
			}

			//If it's lower, we've overflowed.
			else if (tempcheck < tempdigit)
			{
				return -2;
			}

			//If tempcheck == tempdigit, stay spooked.
		}

		//Accumulate current digit with result.
		result += tempdigit * temptensplace;

		//We've gone down one 10s place, so
		//decrement digits.
		--digits;
	}

	return result;
}

