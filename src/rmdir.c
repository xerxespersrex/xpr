//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2023 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _XOPEN_SOURCE 700

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#include "shared/utils.h"

int main(int argc, char *argv[])
{
	bool parents = false;

	int result = 0;

	bool lookingforoptions = true;

	int i = 0;
	int j = 0;

	for (i = 1; i < argc; ++i)
	{
		if (argv[i][0] != '-')
		{
			lookingforoptions = false;
		}

		else if (stringcompare(argv[i], "-"))
		{
			lookingforoptions = false;
		}

		else if (lookingforoptions)
		{
			for (j = 1; argv[i][j] != '\0'; ++j)
			{
				//-p: remove all directories
				//in a pathname.
				if (argv[i][j] == 'p')
				{
					parents = true;
				}

				else
				{
					fprintf(stderr, "rmdir: invalid option %c encountered", argv[i][j]);

					return 1;
				}
			}

			continue;
		}

		if (!parents)
		{
			result = rmdir(argv[i]);

			if (result == -1)
			{
				//TODO: insert checks for errno.
				fprintf(stderr, "rmdir: error removing file %s", argv[i]);

				return 1;
			}
		}

		else
		{
			//TODO: put in parents argument
			fprintf(stderr, "rmdir: -p parents option not implemented yet");

			return 1;
		}
	}

	return 0;
}

