//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2022 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _XOPEN_SOURCE 700

#include <libgen.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "shared/utils.h"

void basenameusage();
int main(int argc, char *argv[]);

void basenameusage()
{
	fprintf(stderr, "Usage: basename string [suffix]\n");

	return;
}

int main(int argc, char *argv[])
{
	//We don't actually take any
	//options for basename, except
	//"--", which makes sense for
	//compatibility reasons.
	bool lookingforoptions = true;

	char *stringbuffer = NULL;
	int stringlength = 0;

	char *returnedbasename = NULL;

	//Counts arguments for sanity.
	int argumentcount = 0;

	//Increment variables.
	int i = 1;
	int j = 0;
	int k = 0;

	for (i = 1; i < argc; ++i)
	{
		if (lookingforoptions)
		{
			if (stringcompare(argv[i], "--"))
			{
				lookingforoptions = false;

				continue;
			}

			lookingforoptions = false;
		}

		++argumentcount;
	}

	if (argumentcount <= 0)
	{
		fprintf(stderr, "basename: no argument given\n");
		basenameusage();

		return 1;
	}

	if (argumentcount > 2)
	{
		fprintf(stderr, "basename: too many arguments given\n");
		basenameusage();

		return 2;
	}

	lookingforoptions = true;

	for (i = 1; i < argc; ++i)
	{
		//Look for "--" in input.
		if (lookingforoptions)
		{
			//If we found it, skip it.
			if (stringcompare(argv[i], "--"))
			{
				lookingforoptions = false;

				continue;
			}

			//Otherwise stop looking.
			lookingforoptions = false;
		}

		//Count the size of the string argument.
		for (stringlength = 0; argv[i][stringlength] != '\0'; ++stringlength)
		{
			//Do nothing.
		}

		++stringlength;

		//Create buffer for copy of the string.
		//We give it some extra padding because
		//basename() uses the same area in memory
		//for its result as its input and in some
		//cases (like a null string) the result
		//is bigger than the input. +20 should
		//be more than safe.
		//
		//The whole reason we create
		//a buffer to pass to basename() is so
		//that we don't unnecessarily mangle
		//the arguments originally passed to
		//basename. (The basename() function
		//changes the input string directly.)
		stringbuffer = (char*)calloc(stringlength + 20, sizeof(char));

		if (stringbuffer == NULL)
		{
			fprintf(stderr, "touch: error creating string buffer for processing; aborting\n");

			return 3;
		}

		//Copy string over.
		for (j = 0; argv[i][j] != '\0'; ++j)
		{
			stringbuffer[j] = argv[i][j];
		}

		stringbuffer[j] = '\0';

		//Use basename() function
		//(XSI extension).
		returnedbasename = basename(stringbuffer);

		if (returnedbasename == NULL)
		{
			fprintf(stderr, "basename: XSI basename() function returned null on %s; exiting\n", argv[i]);

			return 15;
		}

		//Copy over returnedbasename to stringbuffer.
		//(It may in reality just be copying data
		//from later in the heap to earlier in the
		//same area in memory, but that's fine.)
		for (j = 0; returnedbasename[j] != '\0'; ++j)
		{
			stringbuffer[j] = returnedbasename[j];
		}

		stringbuffer[j] = '\0';

		//If there's a suffix argument,
		//remove the suffix.
		if ((i + 1) < argc)
		{
			//Find the end of the suffix.
			for (k = 0; argv[i + 1][k] != '\0'; ++k)
			{
				//Do nothing.
			}

			//Count down until we reach
			//the end of one of the strings
			//or we have a mismatch.
			//(j is already at the end of
			//the stringbuffer from the
			//last loop.)
			while ((j >= 0) && (k >= 0) &&
			       (stringbuffer[j] == argv[i + 1][k]))
			{
				--j;
				--k;
			}

			//If we found every character
			//in the suffix and it wasn't
			//the entire string buffer,
			//remove the suffix.
			if ((j >= 0) && (k < 0))
			{
				stringbuffer[j + 1] = '\0';
			}
		}

		//Finally, print the string.
		printf("%s\n", stringbuffer);

		//Free the stringbuffer.
		free(stringbuffer);

		//We don't want to run this
		//same process on the suffix,
		//so we break.
		break;
	}

	return 0;
}

