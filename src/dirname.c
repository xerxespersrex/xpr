//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2022 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _XOPEN_SOURCE 700

#include <libgen.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "shared/utils.h"

void dirnameusage();
int main(int argc, char *argv[]);

void dirnameusage()
{
	fprintf(stderr, "Usage: dirname string\n");

	return;
}

int main(int argc, char *argv[])
{
	//We don't actually take any
	//options for dirname, except
	//"--", which makes sense for
	//compatibility reasons.
	bool lookingforoptions = true;

	//Buffer for the string.
	char *stringbuffer = NULL;
	int stringlength = 0;

	//Counts arguments for sanity.
	int argumentcount = 0;

	//Increment variables.
	int i = 1;
	int j = 1;

	for (i = 1; i < argc; ++i)
	{
		if (lookingforoptions)
		{
			if (stringcompare(argv[i], "--"))
			{
				lookingforoptions = false;

				continue;
			}

			lookingforoptions = false;
		}

		++argumentcount;
	}

	if (argumentcount <= 0)
	{
		fprintf(stderr, "dirname: no argument given\n");
		dirnameusage();

		return 1;
	}

	if (argumentcount > 1)
	{
		fprintf(stderr, "dirname: too many arguments given\n");
		dirnameusage();

		return 2;
	}

	lookingforoptions = true;

	for (i = 1; i < argc; ++i)
	{
		//Look for "--" in input.
		if (lookingforoptions)
		{
			//If we found it, skip it.
			if (stringcompare(argv[i], "--"))
			{
				lookingforoptions = false;

				continue;
			}

			//Otherwise stop looking.
			lookingforoptions = false;
		}

		//Count the size of the string argument.
		for (stringlength = 0; argv[i][stringlength] != '\0'; ++stringlength)
		{
			//Do nothing.
		}

		++stringlength;

		//Create buffer for copy of the string.
		stringbuffer = (char*)calloc(stringlength, sizeof(char));

		if (stringbuffer == NULL)
		{
			fprintf(stderr, "touch: error creating string buffer for processing; aborting\n");

			return 3;
		}

		//Copy string to buffer.
		for (j = 0; j < stringlength; ++j)
		{
			stringbuffer[j] = argv[i][j];
		}

		//Use dirname() (XSI extension) and
		//print the result.
		printf("%s\n", dirname(stringbuffer));

		//Free the stringbuffer.
		free(stringbuffer);

		//Break is unnecessary, but
		//why not be safe.
		break;
	}

	return 0;
}

