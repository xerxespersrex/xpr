//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2022 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _XOPEN_SOURCE 700

#include <limits.h>
#include <stdbool.h>
#include <stdio.h>

#include "shared/utils.h"

int main(int argc, char *argv[]);
int printlinesfromstdin(const int linestoprint);

//Main.
int main(int argc, char *argv[])
{
	//Pointer to current open file.
	FILE *fptr = NULL;

	//Counts arguments on dry run.
	int argumentcount = 0;

	//Marks if we've printed the first
	//argument yet.
	bool argument_one = true;

	//State of looking for options.
	bool lookingforoptions = true;

	//State of looking for a line count
	//(argument after -n).
	bool lookingforlines = false;

	//Amount of lines to print. Changed
	//by -n option. Defaults to 10.
	int linestoprint = 10;

	//Whether or not we're printing from
	//stdin instead of a file. Set when
	//we see a "-" argument
	//(e.g. "head -").
	bool standardinput = false;

	//Counts lines that have been printed
	int newlinecount = 0;

	//Stores current byte from file/stdin.
	int c = EOF;

	//Increment variables.
	int i = 1;
	int j = 1;

	//Counts real, non-line-count arguments
	//given to the program so that it can
	//format the output correctly with headers
	//if there's multiple.
	for (i = 1; i < argc; ++i)
	{
		if (lookingforlines)
		{
			lookingforlines = false;

			continue;
		}

		if (argv[i][0] != '-')
		{
			lookingforoptions = false;
		}

		else if ((argv[i][0] == '-') && (argv[i][1] == '\0'))
		{
			lookingforoptions = false;
		}

		else if (lookingforoptions)
		{
			for (j = 1; argv[i][j] != '\0'; ++j)
			{
				if (argv[i][j] == 'n')
				{
					lookingforlines = true;
				}
			}

			continue;
		}

		++argumentcount;
	}

	//If on the first pass we couldn't find
	//a line count after -n, error out.
	if (lookingforlines)
	{
		fprintf(stderr, "head: no line count given after -n\n");

		return 3;
	}

	//Reset these from our dry run above.
	lookingforlines = false;
	lookingforoptions = true;

	//Main program logic.
	for (i = 1; i < argc; ++i)
	{
		//If we're in the state where we expect the next
		//argument is the line count to print (after
		//the user passes the -n option), try to convert
		//the argument to an int and set the line count
		//to print to its value.
		if (lookingforlines)
		{
			//We found the linecount.
			lookingforlines = false;

			//Convert argument to int and set
			//linestoprint to that int value.
			linestoprint = stringtonum(argv[i]);

			//Check for errors.
			if ((linestoprint == -1) || (linestoprint == -2))
			{
				fprintf(stderr, "head: unable to convert %s to a valid line count; too big\n", argv[i]);

				return 24;
			}

			if (linestoprint == -3)
			{
				fprintf(stderr, "head: input line count %s is invalid; should not be able to reach this error\n", argv[i]);

				return 25;
			}

			if (linestoprint == -10)
			{
				fprintf(stderr, "head: error converting character in input line count %s to digit\n", argv[i]);

				return 30;
			}

			if (linestoprint < 0)
			{
				fprintf(stderr, "head: input line count %s is invalid for unspecified reason\n", argv[i]);

				return 26;
			}

			continue;
		}

		//If we're not in that special state, the first time
		//we encounter an argument that doesn't start with "-",
		//we are no longer in the option state. Turn it off.
		if (argv[i][0] != '-')
		{
			lookingforoptions = false;
		}

		//If we get the argument of just "-", like "head -",
		//then we print from stdin. So, we turn on the
		//standardinput flag so we know to not try and
		//open a file. We're also not in the option state
		//anymore either.
		else if (argv[i][1] == '\0')
		{
			lookingforoptions = false;

			standardinput = true;
		}

		//If we have more options and we're in the option state...
		else if (lookingforoptions)
		{
			//Set flags based off of the command line options.
			for (j = 1; argv[i][j] != '\0'; ++j)
			{
				//-n: takes next argument as line count
				//to print.
				if (argv[i][j] == 'n')
				{
					lookingforlines = true;
				}
			}

			continue;
		}

		//Print file headers, if multiple arguments.
		if (argumentcount >= 2)
		{
			if (!argument_one)
			{
				printf("\n");
			}

			else
			{
				argument_one = false;
			}

			if (standardinput)
			{
				printf("==> standard input <==\n");
			}

			else
			{
				printf("==> %s <==\n", argv[i]);
			}
		}

		//In the special case that we had -n 0,
		//we don't print the file,
		//we want to (potentially) print the headers
		//and nothing else.
		if (linestoprint <= 0)
		{
			continue;
		}

		//If standardinput is set to true,
		//print from stdin.
		if (standardinput)
		{
			standardinput = false;

			printlinesfromstdin(linestoprint);

			continue;
		}

		//Otherwise, try to open the file
		//named by the argument.
		fptr = fopen(argv[i], "rb");

		if (fptr == NULL)
		{
			fprintf(stderr, "head: could not read input file %s, skipping\n", argv[i]);

			continue;
		}

		//Counts our progress in the file.
		newlinecount = 0;

		//Print lines of the file.
		for (;;)
		{
			c = fgetc(fptr);

			if (ferror(fptr))
			{
				fprintf(stderr, "head: error reading file %s\n", argv[i]);

				break;
			}

			if ((char)c == '\n')
			{
				++newlinecount;
			}

			else if (c == EOF)
			{
				break;
			}

			printf("%c", (char)c);

			if (newlinecount >= linestoprint)
			{
				break;
			}
		}

		//Remember to close the file.
		fclose(fptr);

		fptr = NULL;
	}

	//If no arguments, print from stdin.
	if (argumentcount <= 0)
	{
		printlinesfromstdin(linestoprint);
	}

	return 0;
}

//Prints linestoprint lines from standard input.
int printlinesfromstdin(const int linestoprint)
{
	//Current byte being read from stdin.
	int c = EOF;

	//Count of newlines encountered so far.
	int newlinecount = 0;

	//Required if there's multiple prints
	//from stdin, because EOF is sticky on
	//stdin and needs to be manually
	//cleared (POSIX standard).
	clearerr(stdin);

	//Get stdin and print it until we
	//reach linestoprint or EOF.
	for (;;)
	{
		c = getchar();

		if (c == EOF)
		{
			break;
		}

		if ((char)c == '\n')
		{
			++newlinecount;
		}

		printf("%c", (char)c);

		if (newlinecount >= linestoprint)
		{
			break;
		}
	}

	return 0;
}

