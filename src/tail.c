//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2022 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _XOPEN_SOURCE 700

#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "shared/utils.h"

//FIXME: find out what an appropriate
//memory block size should be for the
//system. Right now we just guess 1024.
//512 was also considered for being
//the standard sector size (but we're
//not touching disk with this).
#define CHUNKSIZE 1024

int main(int argc, char *argv[]);
int tailfile(FILE *const fptr, const int lineorbyte, const int startorend,
	     int startingpoint, const bool follow);
int tailstdin(const int lineorbyte, const int startorend, int startingpoint);

//Main.
int main(int argc, char *argv[])
{
	//Points to stream for current file.
	FILE *fptr = NULL;

	//Counts non-option arguments on command line.
	int argumentcount = 0;

	//When we print the tail of multiple files, it's
	//formatted in such a way that the first tail does
	//not need a newline, but later ones do. This keeps
	//track of that.
	bool argumentone = true;

	//Increment variables for loops.
	int i = 1;
	int j = 0;
	int k = 0;

	//Flag for if we've received "-" as an argument
	//and need to read from stdin.
	bool standardinput = false;

	//Flags for states of looking for options/counts.
	bool lookingforoptions = true;
	bool lookingforlines = false;
	bool lookingforbytes = false;

	//lineorbyte: >= 0, count by lines,
	//< 0, count by bytes.
	//0 default.
	int lineorbyte = 0;

	//startorend: > 0, count from start,
	//<= 0, count from end.
	//0 default.
	int startorend = 0;

	//The byte/line we're starting from,
	//in conjunciton with the above two values.
	//10 default.
	int startingpoint = 10;

	//Whether or not we follow the file (-f option).
	bool follow = false;

	//Holds error code returned from helper functions.
	int errorcode = 0;

	//We need this buffer to remove a leading + or -
	//from byte or line counts after -n/-c options
	//in order to convert to an int.
	char *argbuffer = NULL;

	//Count arguments (so we know how to format output).
	for (i = 1; i < argc; ++i)
	{
		if (lookingforlines || lookingforbytes)
		{
			lookingforlines = false;
			lookingforbytes = false;

			continue;
		}

		if (argv[i][0] != '-')
		{
			lookingforoptions = false;
		}

		else if (argv[i][1] == '\0')
		{
			lookingforoptions = false;
		}

		else if (lookingforoptions)
		{
			for (j = 1; argv[i][j] != '\0'; ++j)
			{
				if (argv[i][j] == 'n')
				{
					lookingforlines = true;
				}

				else if (argv[i][j] == 'c')
				{
					lookingforbytes = true;
				}

				else if (argv[i][j] == 'f')
				{
					//Do nothing.
				}
			}

			continue;
		}

		++argumentcount;
	}

	//If on the first pass we didn't find a
	//matching argument for -n or -c, fail.
	if (lookingforlines || lookingforbytes)
	{
		fprintf(stderr, "tail: no line/byte count given after -n/-c flag");

		return 4;
	}

	lookingforoptions = true;
	lookingforlines = false;
	lookingforbytes = false;
	follow = false;
	standardinput = false;

	//Main logic.
	for (i = 1; i < argc; ++i)
	{
		if (lookingforlines || lookingforbytes)
		{
			if (lookingforlines && lookingforbytes)
			{
				fprintf(stderr, "tail: looking for byte count and line count in same argument; quitting\n");

				return 5;
			}

			lineorbyte = 0;

			if (lookingforlines)
			{
				lineorbyte = 1;
			}

			else if (lookingforbytes)
			{
				lineorbyte = -1;
			}

			lookingforlines = false;
			lookingforbytes = false;

			if (argv[i][0] == '\0')
			{
				fprintf(stderr, "tail: invalid line count; empty argument was encountered\n");

				return 4;
			}

			startorend = 0;

			if (argv[i][0] == '+')
			{
				startorend = 1;
			}

			else if (argv[i][0] == '-')
			{
				startorend = -1;
			}

			//Figure out the size of the argument minus one.
			for (j = 0; argv[i][j] != '\0'; ++j)
			{
				//Do nothing.
			}

			//If we don't have a leading +/-, add one to
			//the size; otherwise, we have the right size.
			if (startorend == 0)
			{
				++j;
			}

			//Buffer to hold argument with leading +/- removed.
			argbuffer = (char*)calloc(j, sizeof(char));

			if (startorend == 0)
			{
				j = 0;
			}

			//If we have leading +/-, start copying after it.
			else
			{
				j = 1;
			}

			//Copy the argument to the buffer.
			k = 0;

			for (; argv[i][j] != '\0'; ++j)
			{
				argbuffer[k] = argv[i][j];

				++k;
			}

			argbuffer[k] = '\0';

			//Convert to int.
			startingpoint = stringtonum(argbuffer);

			//Immediately free buffer.
			free(argbuffer);

			//If we have +0 or -0, POSIX says throw an error.
			if ((startingpoint == 0) && (startorend != 0))
			{
				fprintf(stderr, "tail: starting line of 0 not supported with +/- operators: %s\n", argv[i]);

				return 7;
			}

			//If we got an error, fail.
			if (startingpoint < 0)
			{
				fprintf(stderr, "tail: invalid line/byte count given: %s\n", argv[i]);

				return 9;
			}

			//Next argument.
			continue;
		}

		if (argv[i][0] != '-')
		{
			lookingforoptions = false;
		}

		else if (argv[i][1] == '\0')
		{
			lookingforoptions = false;

			standardinput = true;
		}

		else if (lookingforoptions)
		{
			//Deal with command line flags.
			for (j = 1; argv[i][j] != '\0'; ++j)
			{
				//-n: tail a certain amount of lines.
				//Can be relative to the top or bottom
				//by passing a leading + or -;
				//for example, +5 prints starting
				//from line 5 of the file downwards.
				if (argv[i][j] == 'n')
				{
					//Takes next argument to know where to
					//print from.
					lookingforlines = true;
				}

				//-c: same behavior as -n, except counts
				//bytes instead of lines.
				else if (argv[i][j] == 'c')
				{
					//Takes next argument to know where to
					//print from.
					lookingforbytes = true;
				}

				//-f: "follows" a file indefinitely (until
				//Ctrl-C interrupt). This implementation
				//does not allow -f on stdin, and if multiple
				//files are passed as input, it only works on
				//the last file (same as GNU tail). It does work
				//if it's passed stdin in the middle or beginning
				//of the arguments as -, and then as the final
				//argument is given a file.
				else if (argv[i][j] == 'f')
				{
					follow = true;
				}
			}

			//If we get -n and -c together,
			//we don't know what to do, so throw an error.
			if (lookingforlines && lookingforbytes)
			{
				fprintf(stderr, "tail: looking for both lines and bytes on next argument; invalid use of flags\n");

				return 8;
			}

			continue;
		}

		//Print pretty headers if there's multiple files.
		if (argumentcount >= 2)
		{
			if (!argumentone)
			{
				printf("\n");
			}

			else
			{
				argumentone = false;
			}

			if (standardinput)
			{
				printf("==> standard input <==\n");
			}

			else
			{
				printf("==> %s <==\n", argv[i]);
			}
		}

		//If we got "-" as an argument...
		if (standardinput)
		{
			standardinput = false;

			errorcode = 0;

			//Tail from stdin.
			errorcode = tailstdin(lineorbyte, startorend, startingpoint);

			if (errorcode != 0)
			{
				fprintf(stderr, "tail: error reading from stdin, skipping\n");
			}

			continue;
		}

		//Open file.
		fptr = fopen(argv[i], "rb");

		if (fptr == NULL)
		{
			fprintf(stderr, "tail: could not read input file %s, skipping\n", argv[i]);

			continue;
		}

		errorcode = 0;

		//Only honor -f on the last argument,
		//and only if it's not stdin.
		if (i == (argc - 1))
		{
			//Tail last file (follow if -f).
			errorcode = tailfile(fptr, lineorbyte, startorend, startingpoint, follow);
		}

		else
		{
			//Tail any other file (never -f).
			errorcode = tailfile(fptr, lineorbyte, startorend, startingpoint, false);
		}

		if (errorcode != 0)
		{
			fprintf(stderr, "tail: error printing file %s\n", argv[i]);
		}

		//Remember to close the file!
		fclose(fptr);

		fptr = NULL;
	}

	//If we didn't reach an argument, default behavior
	//is to tail from stdin. We needed to do the loop
	//first though so that we could turn on any flags
	//that would affect how we tail stdin.
	if (argumentcount <= 0)
	{
		errorcode = 0;

		//Tail stdin.
		errorcode = tailstdin(lineorbyte, startorend, startingpoint);

		if (errorcode != 0)
		{
			fprintf(stderr, "tail: error reading from stdin\n");

			return 1;
		}
	}

	return 0;
}

int tailfile(FILE *const fptr, const int lineorbyte, const int startorend,
	     int startingpoint, const bool follow)
{
	//Current byte from file.
	int c = EOF;

	//Keeps track of total bytes in file.
	int filesize = 0;

	//Keeps track of total newlines in file.
	int newlines = 0;

	//Whether or not we print; turned on when
	//we reach the point to print determined
	//by the function arguments.
	bool print = false;

	//Increment variable.
	int i = 0;

	//Keeps track of if the file had a trailing newline.
	//If not, we need to add one.
	bool lastwasnewline = false;

	//If we get -n 0, print nothing.
	if ((lineorbyte >= 0) && (startorend == 0) && (startingpoint == 0))
	{
		return 0;
	}

	//Check if the file pointer is valid.
	if (fptr == NULL)
	{
		return 1;
	}

	//Count bytes/newlines in file.
	for (;;)
	{
		c = fgetc(fptr);

		if (c == EOF)
		{
			if (!lastwasnewline)
			{
				++newlines;
			}

			break;
		}

		if (ferror(fptr))
		{
			return 2;
		}

		if ((char)c == '\n')
		{
			lastwasnewline = true;

			++newlines;
		}

		else
		{
			lastwasnewline = false;
		}

		++filesize;
	}

	//If the file is empty (and we're not following),
	//we're done.
	if (filesize == 0 && !follow)
	{
		return 0;
	}

	//If we're counting from the end..
	if (startorend <= 0)
	{
		//If we're counting by lines..
		if (lineorbyte >= 0)
		{
			startingpoint = newlines - startingpoint;
		}

		//If we're counting by bytes...
		else
		{
			startingpoint = filesize - startingpoint;
		}
	}

	//If we're counting from the beginning...
	else
	{
		--startingpoint;
	}

	if (startingpoint < 0)
	{
		startingpoint = 0;
	}

	fseek(fptr, 0, SEEK_SET);

	newlines = 0;

	i = 0;

	for (;;)
	{
		c = fgetc(fptr);

		if (ferror(fptr))
		{
			return 87;
		}

		if (c == EOF)
		{
			if (follow)
			{
				//This isn't portable outside of
				//POSIX systems, but we're aiming
				//for POSIX anyways, so we don't
				//care.
				sleep(1);

				clearerr(fptr);

				continue;
			}

			break;
		}

		//Determine if we should start printing.
		if (!print)
		{
			if (lineorbyte >= 0)
			{
				if (newlines >= startingpoint)
				{
					//If we've passed enough newlines,
					//start printing.
					print = true;
				}
			}

			else
			{
				if (i >= startingpoint)
				{
					//If we've passed enough bytes,
					//start printing.
					print = true;
				}
			}
		}

		//If we should, print.
		if (print)
		{
			printf("%c", (char)c);
		}

		//Count our progress in newlines/bytes in the file.
		if ((char)c == '\n')
		{
			++newlines;
		}

		++i;
	}

	return 0;
}

int tailstdin(const int lineorbyte, const int startorend, int startingpoint)
{
	//Current byte read from stdin.
	int c = EOF;

	//Our stdin buffer is >= this value.
	//Also keeps track of size of stdin
	//exactly.
	int buffersize = 0;

	//Increment variable.
	int i = 0;

	//Counts newlines.
	int newlines = 0;

	//How many "chunks" of memory we've
	//consumed. The size of the chunks
	//is determined by CHUNKSIZE macro.
	int chunks = 0;

	//Pointer to the char buffer for stdin.
	char *stdinbuffer = NULL;

	//Becomes true when we reach the point
	//in the standard input that we start
	//printing from.
	bool print = false;

	//If we get input that's missing a trailing
	//newline, we need this to correctly count
	//the lines of the file.
	bool lastwasnewline = false;

	//EOF is sticky on stdin; we need to clear
	//it in case we read from stdin multiple times
	//(e.g. "tail -n +1 - -").
	clearerr(stdin);

	//Read stdin to buffer.
	for (;;)
	{
		c = getchar();

		//Careful, we don't give the buffer a trailing
		//null byte \0! But it's okay, because we're
		//printing character-by-character, and we know
		//the buffer's exact length. This may be better
		//because I suppose it's possible for the input
		//to contain null bytes as well.
		if (c == EOF)
		{
			//If we're missing a trailing newline, we've
			//still encountered a line, it's just not
			//formatted properly. We'll treat it
			//as a line by adding one to newlines.
			if (!lastwasnewline)
			{
				++newlines;
			}

			break;
		}

		if ((buffersize % CHUNKSIZE) == 0)
		{
			++chunks;

			stdinbuffer = (char*)realloc(stdinbuffer, CHUNKSIZE * chunks * sizeof(char));

			if (stdinbuffer == NULL)
			{
				return 1;
			}
		}

		if ((char)c == '\n')
		{
			lastwasnewline = true;

			++newlines;
		}

		else
		{
			lastwasnewline = false;
		}

		stdinbuffer[buffersize] = (char)c;

		++buffersize;
	}

	//If there was no standard
	//input besides EOF, we're
	//done.
	if (buffersize == 0)
	{
		return 0;
	}

	//If we get -n 0, print nothing.
	//We needed to read stdin though
	//because otherwise it'd be left
	//pending in the stream.
	if ((lineorbyte >= 0) && (startorend == 0) && (startingpoint == 0))
	{
		//Remember to free the buffer!
		free(stdinbuffer);

		return 0;
	}

	//If we're counting from the end..
	if (startorend <= 0)
	{
		//If we're counting by lines, subtract the input
		//starting point from the total newlines.
		if (lineorbyte >= 0)
		{
			startingpoint = newlines - startingpoint;
		}

		//If we're counting by bytes, same thing but with bytes.
		else
		{
			startingpoint = buffersize - startingpoint;
		}
	}

	//If we're counting from the beginning,
	//we just need to adjust down one.
	else
	{
		--startingpoint;
	}

	//If our starting point is before the beginning of the file,
	//clamp it to the start of the file.
	if (startingpoint < 0)
	{
		startingpoint = 0;
	}

	//tail logic.
	newlines = 0;

	for (i = 0; i < buffersize; ++i)
	{
		if (!print)
		{
			if (lineorbyte >= 0)
			{
				if (newlines >= startingpoint)
				{
					//If we've passed enough newlines,
					//start printing.
					print = true;
				}
			}

			else
			{
				if (i >= startingpoint)
				{
					//If we've passed enough bytes,
					//start printing.
					print = true;
				}
			}
		}

		if (print)
		{
			printf("%c", stdinbuffer[i]);
		}

		if (stdinbuffer[i] == '\n')
		{
			++newlines;
		}
	}

	//Remember to free the buffer!
	free(stdinbuffer);

	return 0;
}

