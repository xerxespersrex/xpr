//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2022 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _XOPEN_SOURCE 700

#include <stdbool.h>
#include <stdio.h>

#include "shared/utils.h"

int main(int argc, char *argv[]);
int countstdin(int *const counts);
int countfile(int *const counts, FILE *const fptr);

int countfile(int *const counts, FILE *const fptr)
{
	int c = EOF;

	bool waitingforword = true;

	int i = 0;

	for (i = 0; i < 4; ++i)
	{
		counts[i] = 0;
	}

	for (;;)
	{
		c = fgetc(fptr);

		if (ferror(fptr))
		{
			return 1;
		}

		if (c == EOF)
		{
			break;
		}

		if (iswhitespace((char)c))
		{
			if ((char)c == '\n')
			{
				++counts[0];
			}

			waitingforword = true;
		}

		else if (waitingforword == true)
		{
			waitingforword = false;

			++counts[1];
		}

		++counts[3];
	}

	return 0;
}

int countstdin(int *const counts)
{
	int c = EOF;

	bool waitingforword = true;

	int i = 0;

	clearerr(stdin);

	for (i = 0; i < 4; ++i)
	{
		counts[i] = 0;
	}

	for (;;)
	{
		c = getchar();

		if (c == EOF)
		{
			break;
		}

		if (iswhitespace((char)c))
		{
			if ((char)c == '\n')
			{
				++counts[0];
			}

			waitingforword = true;
		}

		else if (waitingforword == true)
		{
			waitingforword = false;

			++counts[1];
		}

		++counts[3];
	}

	return 0;
}

int main(int argc, char *argv[])
{
	FILE *fptr = NULL;

	bool standardinput = false;

	//Stores counts.
	//counts[0] is line count.
	//counts[1] is word count.
	//counts[2] is character count.
	//counts[3] is byte count.
	int counts[4] = {0, 0, 0, 0};

	//Stores summed counts.
	int totalcounts[4] = {0, 0, 0, 0};

	bool lookingforoptions = true;

	bool printall = true;
	bool printlines = false;
	bool printwords = false;
	bool printbytes = false;

	int argumentcount = 0;

	int i = 1;
	int j = 1;

	for (i = 1; i < argc; ++i)
	{
		if (argv[i][0] != '-')
		{
			lookingforoptions = false;
		}

		else if (argv[i][1] == '\0')
		{
			lookingforoptions = false;
		}

		else if (lookingforoptions)
		{
			continue;
		}

		++argumentcount;
	}

	lookingforoptions = true;

	for (i = 1; i < argc; ++i)
	{
		if (argv[i][0] != '-')
		{
			lookingforoptions = false;
		}

		else if (argv[i][1] == '\0')
		{
			lookingforoptions = false;

			standardinput = true;
		}

		else if (lookingforoptions)
		{
			for (j = 1; argv[i][j] != '\0'; ++j)
			{
				//-l: print line count.
				if (argv[i][j] == 'l')
				{
					printall = false;
					printlines = true;
				}

				//-w: print word count.
				else if (argv[i][j] == 'w')
				{
					printall = false;
					printwords = true;
				}

				//-c: print byte count.
				else if (argv[i][j] == 'c')
				{
					printall = false;
					printbytes = true;
				}

				//FIXME
				//-m: print character count.
				else if (argv[i][j] == 'm')
				{
					fprintf(stderr, "wc: -m flag not yet supported\n");

					return 22;
				}
			}

			continue;
		}

		if (standardinput)
		{
			standardinput = false;

			countstdin(counts);
		}

		else
		{
			fptr = fopen(argv[i], "rb");

			if (fptr == NULL)
			{
				fprintf(stderr, "wc: could not read input file %s, skipping\n", argv[i]);

				continue;
			}

			//Count file; if there's an error,
			//print to stderr, close file, and
			//continue.
			if (countfile(counts, fptr) != 0)
			{
				fprintf(stderr, "wc: error reading file %s, skipping\n", argv[i]);

				fclose(fptr);

				fptr = NULL;

				continue;
			}

			fclose(fptr);

			fptr = NULL;
		}

		if (printall || printlines)
		{
			printf("%d ", counts[0]);
		}

		if (printall || printwords)
		{
			printf("%d ", counts[1]);
		}

		if (printall || printbytes)
		{
			printf("%d ", counts[3]);
		}

		printf("%s\n", argv[i]);

		for (j = 0; j < 4; ++j)
		{
			totalcounts[j] += counts[j];
		}
	}

	if (argumentcount >= 2)
	{
		if (printall || printlines)
		{
			printf("%d ", totalcounts[0]);
		}

		if (printall || printwords)
		{
			printf("%d ", totalcounts[1]);
		}

		if (printall || printbytes)
		{
			printf("%d ", totalcounts[3]);
		}

		printf("total\n");
	}

	else if (argumentcount == 0)
	{
		countstdin(counts);

		if (printall || printlines)
		{
			printf("%d ", counts[0]);
		}

		if (printall || printwords)
		{
			printf("%d ", counts[1]);
		}

		if (printall || printbytes)
		{
			printf("%d ", counts[3]);
		}

		printf("\n");
	}

	return 0;
}

