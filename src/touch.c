//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2022, 2023 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _XOPEN_SOURCE 700

#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "shared/century.h"
#include "shared/utils.h"

int createtimespec(int year, int month, int day, int hours, int minutes, int seconds,
		   long nanoseconds, bool timeinutc, struct timespec *const restrict result);
int datetimeconvert(char *const string, struct timespec *const restrict result);
int main(int argc, char *argv[]);
int timeconvert(char *const string, struct timespec *const restrict result);
void touchusage();

int createtimespec(int year, int month, int day, int hours, int minutes, int seconds,
		   long nanoseconds, bool timeinutc, struct timespec *const restrict result)
{
	struct tm brokendowntime;

	//tm.tm_year is stored as the amount
	//of years since 1900, so we subtract
	//1900 from our year.
	brokendowntime.tm_year = year - 1900;

	//tm.tm_mon stores the month from 0-11,
	//so we subtract 1 here.
	brokendowntime.tm_mon = month - 1;

	brokendowntime.tm_mday = day;
	brokendowntime.tm_hour = hours;
	brokendowntime.tm_min = minutes;
	brokendowntime.tm_sec = seconds;

	//Trying to set this based off of the
	//extern int daylight set by tzset()
	//was not successful. -1 determines
	//the local timezone's dst setting
	//automatically.
	brokendowntime.tm_isdst = -1;

	result->tv_sec = mktime(&brokendowntime);
	result->tv_nsec = nanoseconds;

	//This is the only sure-fire way to
	//detect an error creating the
	//timestamp, if mktime actually
	//throws an error. On GNU/Linux,
	//overflows/underflows are not
	//actually caught by this.
	if ((result->tv_sec == (time_t)(-1)) && (errno == EOVERFLOW))
	{
		fprintf(stderr, "touch: error encountered with mktime() in createtimespec() function\n");

		return 41;
	}

	//If we're at the highest or lowest possible value for time_t,
	//the chances of having that exact second set by the user
	//are extremely low. Instead, we'll assume that the user
	//set a time that, when converted by mktime(), was
	//higher or lower than the maximum value of time_t; in that
	//case, POSIX says we fail with an error.
	//
	//On GNU/Linux, there's apparently some padding to time_t's
	//max and minimum such that this never seems to succeed.
	//As such, it's not possible (at least, not easily) to
	//determine if our date is going to just get truncated
	//down to the year 2446 or something.
	if (((result->tv_sec + (time_t)(1)) < result->tv_sec) ||
	    ((result->tv_sec - (time_t)(1)) > result->tv_sec))
	{
		fprintf(stderr, "touch: unable to set time; exceeds the maximum + or - value of seconds since the Epoch\n");

		return 43;
	}

	if (timeinutc)
	{
		//We're going to check for
		//overflow with this timezone change;
		//if we get an overflow (or underflow),
		//POSIX says we fail with an error.
		//
		//Again, on GNU/Linux there's padding
		//and this will never succeed.
		if (((timezone > (time_t)(0)) &&
		     ((result->tv_sec - timezone) > result->tv_sec)) ||
		    ((timezone < (time_t)(0)) &&
		     ((result->tv_sec - timezone) < result->tv_sec)))
		{
			fprintf(stderr, "touch: unable to set time; exceeds the maximum + or - value of seconds since the Epoch\n");

			return 45;
		}

		//If there's not an overflow, just
		//subtract the value of the extern
		//variable timezone set by tzset()
		//from our seconds since the Epoch.
		//(i.e. EST - TZ = UTC).
		//
		//timezone is an extern variable that
		//should have been set by tzset()
		//being called implicitly by mktime()
		//above.
		result->tv_sec -= timezone;
	}

	return 0;
}

//Consider using strptime() instead?
int datetimeconvert(char *const string, struct timespec *const restrict result)
{
	//Holds our status in parsing
	//the date string.
	bool inyear = true;
	bool inmonth = false;
	bool inday = false;
	bool inhours = false;
	bool inminutes = false;
	bool inseconds = false;
	bool infracseconds = false;

	//Set to true if the string
	//was parsed 100% successfully.
	bool success = false;

	//Stores the digits of each
	//section of the input string.
	int yeardigits = 0;
	int monthdigits = 0;
	int daydigits = 0;
	int hourdigits = 0;
	int minutedigits = 0;
	int seconddigits = 0;
	int fracdigits = 0;

	//Stores the parts of the
	//date that will be converted
	//to a timespec using the
	//createtimespec() function.
	int year = 0;
	int month = 0;
	int day = 0;
	int hours = 0;
	int minutes = 0;
	int seconds = 0;
	long nanoseconds = 0;

	//Whether or not we have to
	//convert UTC to local time.
	bool timeinutc = false;

	//Temp variable to simplify
	//calculations later.
	int temp = 0;

	//Increment variable.
	int i = 0;

	//Parse the input string byte-by-byte.
	//(I'm not smart enough to come up
	//with a clean way to do this.)
	for (i = 0; string[i] != '\0'; ++i)
	{
		if (inyear)
		{
			if (string[i] == '-')
			{
				//POSIX requires at least four digits for
				//the year. I'm artificially capping it at
				//five because I highly doubt the seconds
				//since Epoch is going to go to six digit
				//years on any system.
				//
				//Completely uncapping it would be possible
				//but requires making sure that the read
				//year does not overflow an int later on.
				if ((yeardigits != 4) && (yeardigits != 5))
				{
					fprintf(stderr, "touch: with -d option argument %s, years is not 4 or 5 digits\n", string);

					return 2;
				}

				inyear = false;
				inmonth = true;

				continue;
			}

			if ((chartonum(string[i]) < 0) || (chartonum(string[i]) > 9))
			{
				fprintf(stderr, "touch: invalid character %c encountered in -d option argument %s\n", string[i], string);

				return 1;
			}

			++yeardigits;

			continue;
		}

		if (inmonth)
		{
			if (string[i] == '-')
			{
				if (monthdigits != 2)
				{
					fprintf(stderr, "touch: months is not exactly two digits: %s\n", string);

					return 4;
				}

				inmonth = false;
				inday = true;

				continue;
			}

			if ((chartonum(string[i]) < 0) || (chartonum(string[i]) > 9))
			{
				fprintf(stderr, "touch: invalid character %c encountered in -d option argument %s\n", string[i], string);

				return 5;
			}

			++monthdigits;

			continue;
		}

		if (inday)
		{
			if ((string[i] == ' ') || (string[i] == 'T'))
			{
				if (daydigits != 2)
				{
					fprintf(stderr, "touch: day is not exactly two digits: %s\n", string);

					return 6;
				}

				inday = false;
				inhours = true;

				continue;
			}

			if ((chartonum(string[i]) < 0) || (chartonum(string[i]) > 9))
			{
				fprintf(stderr, "touch: invalid character %c encountered in -d option argument %s\n", string[i], string);

				return 7;
			}

			++daydigits;

			continue;
		}

		if (inhours)
		{
			if (string[i] == ':')
			{
				if (hourdigits != 2)
				{
					fprintf(stderr, "touch: hour is not exactly two digits: %s\n", string);

					return 9;
				}

				inhours = false;
				inminutes = true;

				continue;
			}

			if ((chartonum(string[i]) < 0) || (chartonum(string[i]) > 9))
			{
				fprintf(stderr, "touch: invalid character %c encountered in -d option argument %s\n", string[i], string);

				return 10;
			}

			++hourdigits;

			continue;
		}

		if (inminutes)
		{
			if (string[i] == ':')
			{
				if (minutedigits != 2)
				{
					fprintf(stderr, "touch: minute is not exactly two digits: %s\n", string);

					return 12;
				}

				inminutes = false;
				inseconds = true;

				continue;
			}

			if ((chartonum(string[i]) < 0) || (chartonum(string[i]) > 9))
			{
				fprintf(stderr, "touch: invalid character %c encountered in -d option argument %s\n", string[i], string);

				return 13;
			}

			++minutedigits;

			continue;
		}

		if (inseconds)
		{
			if (string[i] == 'Z')
			{
				if (seconddigits != 2)
				{
					fprintf(stderr, "touch: second is not exactly two digits: %s\n", string);

					return 14;
				}

				if (string[i + 1] != '\0')
				{
					fprintf(stderr, "touch: encountered invalid character %c in -d option argument %s\n", string[i + 1], string);

					return 15;
				}

				inseconds = false;

				timeinutc = true;

				success = true;

				break;
			}

			if ((string[i] == '.') || (string[i] == ','))
			{
				if (seconddigits != 2)
				{
					fprintf(stderr, "touch: second is not exactly two digits: %s\n", string);

					return 16;
				}

				inseconds = false;
				infracseconds = true;

				continue;
			}

			if ((chartonum(string[i]) < 0) || (chartonum(string[i]) > 9))
			{
				fprintf(stderr, "touch: invalid character %c encountered in -d option argument %s\n", string[i], string);

				return 17;
			}

			++seconddigits;

			if (string[i + 1] == '\0')
			{
				if (seconddigits != 2)
				{
					fprintf(stderr, "touch: second is not exactly two digits: %s\n", string);

					return 87;
				}

				inseconds = false;

				success = true;

				break;
			}

			continue;
		}

		if (infracseconds)
		{
			if (string[i] == 'Z')
			{
				if (string[i + 1] != '\0')
				{
					fprintf(stderr, "touch: encountered T too early in -t option argument: %s\n", string);

					return 18;
				}

				if (fracdigits <= 0 || fracdigits > 9)
				{
					fprintf(stderr, "touch: fractional seconds are not within 1 to 9 digits: %s\n", string);

					return 19;
				}

				infracseconds = false;

				timeinutc = true;

				success = true;

				break;
			}

			if ((chartonum(string[i]) < 0) || (chartonum(string[i]) > 9))
			{
				fprintf(stderr, "touch: invalid character %c encountered in -d option argument %s\n", string[i], string);

				return 21;
			}

			++fracdigits;

			if (string[i + 1] == '\0')
			{
				if ((fracdigits <= 0) || (fracdigits > 9))
				{
					fprintf(stderr, "touch: fractional seconds are not within 1 to 9 digits: %s\n", string);

					return 20;
				}

				infracseconds = false;

				success = true;
			}

			continue;
		}

		fprintf(stderr, "touch: reached unreachable point in code in datetimeconvert() with %s argument\n", string);

		return 25;
	}

	if (!success)
	{
		fprintf(stderr, "touch: failed to successfully parse -d option argument %s\n", string);

		return 27;
	}

	temp = yeardigits;

	for (i = 0; i < temp; ++i)
	{
		year += chartonum(string[i]) * fastexponent(10, (temp - i - 1));
	}

	i = temp + 1;

	temp += 1 + monthdigits;

	for (; i < temp; ++i)
	{
		month += chartonum(string[i]) * fastexponent(10, (temp - i - 1));
	}

	if ((month <= 0) || (month > 12))
	{
		fprintf(stderr, "touch: in -t option argument, month was not in the range 01-12: %s\n", string);

		return 31;
	}

	i = temp + 1;

	temp += 1 + daydigits;

	for (; i < temp; ++i)
	{
		day += chartonum(string[i]) * fastexponent(10, (temp - i - 1));
	}

	if ((day < 1) || (day > 31))
	{
		fprintf(stderr, "touch: in -t option argument, day was not in the range 01-31: %s\n", string);

		return 33;
	}

	i = temp + 1;

	temp += 1 + hourdigits;

	for (; i < temp; ++i)
	{
		hours += chartonum(string[i]) * fastexponent(10, (temp - i - 1));
	}

	if ((hours < 0) || (hours >= 24))
	{
		fprintf(stderr, "touch: in -t option argument, hours was not in the range 00-23: %s\n", string);

		return 35;
	}

	i = temp + 1;

	temp += 1 + minutedigits;

	for (; i < temp; ++i)
	{
		minutes += chartonum(string[i]) * fastexponent(10, (temp - i - 1));
	}

	if ((minutes < 0) || (minutes >= 60))
	{
		fprintf(stderr, "touch: in -t option argument, minutes was not in the range 00-59: %s\n", string);

		return 37;
	}

	i = temp + 1;

	temp += 1 + seconddigits;

	for (; i < temp; ++i)
	{
		seconds += chartonum(string[i]) * fastexponent(10, (temp - i - 1));
	}

	//Seconds goes up to 60 inclusive to account
	//for leap seconds.
	if ((seconds < 0) || (seconds > 60))
	{
		fprintf(stderr, "touch: in -t option argument, seconds was not in the range 00-60: %s\n", string);

		return 39;
	}

	i = temp + 1;

	temp += 1 + fracdigits;

	for (; i < temp; ++i)
	{
		//The maximum value of nanoseconds is 999999999,
		//so we don't need to worry about overflow,
		//but let's cast safely anyways.
		nanoseconds += (long)chartonum(string[i]) * (long)(fastexponent(10, (temp - i - 1)));
	}

	nanoseconds *= (long)(fastexponent(10, 9 - fracdigits));

	if ((nanoseconds < 0L) || (nanoseconds >= 1000000000L))
	{
		fprintf(stderr, "touch: error encountered in -d option, nanoseconds too big: %s\n", string);

		return 40;
	}

	return createtimespec(year, month, day, hours, minutes,
			      seconds, nanoseconds, timeinutc, result);
}

int main(int argc, char *argv[])
{
	int fd = -1;

	int errorcode = -1;

	bool errorstate = false;

	bool create = true;

	struct timespec times[2];

	times[0].tv_sec = (time_t)0;
	times[0].tv_nsec = UTIME_NOW;

	times[1].tv_sec = (time_t)0;
	times[1].tv_nsec = UTIME_NOW;

	struct stat tempstat;

	bool lookingforoptions = true;
	bool lookingforreffile = false;
	bool lookingfordatetime = false;
	bool lookingfortime = false;

	bool changeboth = true;
	bool changeatime = false;
	bool changemtime = false;
	bool timeset = false;
	bool finaloption = false;

	bool havetouched = false;

	int i = 1;
	int j = 1;

	for (i = 1; i < argc; ++i)
	{
		//If we're looking for an option argument,
		//handle it.
		if (lookingforreffile || lookingfordatetime || lookingfortime)
		{
			//If we're looking for more than one, fail.
			if ((lookingforreffile && lookingfordatetime) ||
			    (lookingforreffile && lookingfortime) ||
			    (lookingfordatetime && lookingfortime))
			{
				fprintf(stderr, "touch: looking for a combination of -d/-r/-t flag arguments in the same place; exiting\n");
				touchusage();

				return 3;
			}

			//If we got "--" instead, fail.
			if (stringcompare(argv[i], "--"))
			{
				fprintf(stderr, "touch: no argument given for one of -d/-r/-t flags; reached option delimiter \"--\" instead\n");
				touchusage();

				return 24;
			}

			//-r option
			if (lookingforreffile)
			{
				lookingforreffile = false;
				timeset = true;

				errorcode = 0;

				//Stat the reffile.
				errorcode = stat(argv[i], &tempstat);

				if (errorcode != 0)
				{
					fprintf(stderr, "touch: error getting information from file %s for -r argument, exiting\n", argv[i]);

					return 10;
				}

				//Set the times we're going to change
				//on the touched file to the times of the
				//ref file.
				times[0].tv_sec = tempstat.st_atim.tv_sec;
				times[0].tv_nsec = tempstat.st_atim.tv_nsec;

				times[1].tv_sec = tempstat.st_mtim.tv_sec;
				times[1].tv_nsec = tempstat.st_mtim.tv_nsec;

				continue;
			}

			//-d or -t option arguments
			if (lookingfordatetime || lookingfortime)
			{
				errorcode = 0;

				if (lookingfordatetime)
				{
					//Get timespec from -d option argument
					errorcode = datetimeconvert(argv[i], &times[0]);

					if (errorcode != 0)
					{
						fprintf(stderr, "touch: error converting -d argument %s to a valid time\n", argv[i]);
						touchusage();

						return 8;
					}
				}

				else
				{
					//Get timespec form -t option argument
					errorcode = timeconvert(argv[i], &times[0]);

					if (errorcode != 0)
					{
						fprintf(stderr, "touch: error converting -t argument %s to a valid time\n", argv[i]);
						touchusage();

						return 10;
					}
				}

				lookingfortime = false;
				lookingfordatetime = false;

				//Time before the Epoch is implementation
				//defined, and code relying on it is not
				//guaranteed to be portable. It will work
				//on GNU and GNU/Linux, though.
				if (times[0].tv_sec < 0)
				{
					fprintf(stderr, "touch: warning: behavior for setting time to before the Epoch is implementation-defined; -d/-t option argument %s\n", argv[i]);
				}

				timeset = true;

				//There's no way to specify different times
				//for atime and mtime without running touch
				//twice, so we just set one and then set
				//the other one to be equal to the first.
				times[1] = times[0];

				continue;
			}

			fprintf(stderr, "touch: unreachable point in code, exiting\n");

			return 69;
		}

		//If an argument doesn't start with -, we
		//know it's not an option, so parse it as
		//a file to touch.
		if (argv[i][0] != '-')
		{
			lookingforoptions = false;
		}

		//Argument -, in commands that affect files,
		//is always taken to mean either the file
		//named "-", or stdin. touch does not take
		//stdin in any circumstance, so it's
		//interpreted as the file named -.
		else if (stringcompare(argv[i], "-"))
		{
			lookingforoptions = false;
		}

		//If we start with '-' and we're looking
		//for options still...
		else if (lookingforoptions)
		{
			//Argument -- is the standard delimiter argument
			//between options and non-options; just set
			//lookingforoptions to false and continue.
			if (stringcompare(argv[i], "--"))
			{
				lookingforoptions = false;

				continue;
			}

			//Change behavior based on flags.
			for (j = 1; argv[i][j] != '\0'; ++j)
			{
				if (finaloption)
				{
					fprintf(stderr, "touch: when parsing options in %s, an option accepting an option argument was followed by another option; this is not POSIX compliant\n", argv[i]);
					fprintf(stderr, "For example, \"touch -acr reffile touchfile\" is okay, but \"touch -arc reffile touchfile\" is not\n");
					touchusage();

					return 22;
				}

				//-a: change access time.
				//If -m is not also set,
				//do not change modification
				//time.
				if (argv[i][j] == 'a')
				{
					changeboth = false;
					changeatime = true;
				}

				//-m: change modification time.
				//If -a is not also set,
				//do not change access time.
				else if (argv[i][j] == 'm')
				{
					changeboth = false;
					changemtime = true;
				}

				//-c: don't create files if
				//they don't exist.
				else if (argv[i][j] == 'c')
				{
					create = false;
				}

				//-d: set time based on
				//datetime in next argument.
				else if (argv[i][j] == 'd')
				{
					lookingfordatetime = true;
					finaloption = true;
				}

				//-t: set time based on
				//time in next argument.
				else if (argv[i][j] == 't')
				{
					lookingfortime = true;
					finaloption = true;
				}

				//-r: set time based on
				//the timestamps of a
				//reference file.
				else if (argv[i][j] == 'r')
				{
					lookingforreffile = true;
					finaloption = true;
				}

				else
				{
					fprintf(stderr, "touch: unknown option %c encountered in option argument %s\n", argv[i][j], argv[i]);
					touchusage();

					return 8;
				}
			}

			finaloption = false;

			//If we're looking for more than one option
			//argument in the next argument, automatically
			//fail.
			if ((lookingforreffile && lookingfordatetime) ||
			    (lookingforreffile && lookingfortime) ||
			    (lookingfordatetime && lookingfortime))
			{
				fprintf(stderr, "touch: looking for a combination of -d/-r/-t flag arguments in the same place; exiting\n");
				touchusage();

				return 2;
			}

			continue;
		}

		//If we made it this far, we're definitely going to
		//attempt to touch a file. This tracks whether or not
		//we had arguments to the program so we can print
		//an error message if we didn't.
		havetouched = true;

		//This isn't technically following what the standards
		//say touch should do to the letter, but it also says
		//that your modifications to the file's time should be
		//equivalent, so it doesn't want an exact implementation.
		//
		//The difference here in the create-if-doesn't-exist branch
		//is we don't call creat(), which would be exactly the same
		//as what we have here with open() except it adds the O_TRUNC
		//flag. We don't want this because we want to access the file
		//with a EAFP technique to avoid TOCTOU bugs, and if we pass
		//the O_TRUNC flag when the file exists, we would destroy the
		//contents of the file (on a command that's not even supposed
		//to affect file contents). That's bad!
		//
		//In the case the file exists, we also don't use utimensat()
		//like the standard says because we don't want to invoke
		//TOCTOU bugs, so we're just going to use open and
		//futimens() like we would if the file doesn't exist.
		if (create)
		{
			fd = open(argv[i], (O_WRONLY | O_CREAT), (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH));

			if (fd < 0)
			{
				errorstate = true;

				if (errno == EACCES)
				{
					fprintf(stderr, "touch: could not open/create file %s; access denied\n", argv[i]);
				}

				//I don't know if touch should touch
				//directories or not; the specification
				//uses the word "file" but everything in
				//*nix is technically a file, including
				//directories.
				else if (errno == EISDIR)
				{
					fprintf(stderr, "touch: %s looks to be a directory; this program does not touch directories (the POSIX specification isn't very clear whether it should or not)\n", argv[i]);
					touchusage();
				}

				else
				{
					fprintf(stderr, "touch: unspecified error in create path with argument %s\n", argv[i]);
				}

				continue;
			}
		}

		//This is the same except if the file doesn't exist,
		//we don't create it, and we don't throw an error
		//in that case. (-c flag.)
		else
		{
			fd = open(argv[i], O_WRONLY, (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH));

			if (fd < 0)
			{
				if (errno = ENOENT)
				{
					continue;
				}

				errorstate = true;

				if (errno == EACCES)
				{
					fprintf(stderr, "touch: could not touch file %s; access denied\n", argv[i]);
				}

				else if (errno == EISDIR)
				{
					fprintf(stderr, "touch: %s looks to be a directory; this program does not touch directories (the POSIX specification isn't very clear whether it should or not)\n", argv[i]);
					touchusage();
				}

				else
				{
					fprintf(stderr, "touch: unspecified error in -c path with argument %s\n", argv[i]);
				}

				continue;
			}
		}

		//Set the times to current time (UTIME_NOW)
		//or to be ignored (UTIME_OMIT) depending
		//on how the different flags were set as
		//the options were parsed. Default behavior
		//is to set both atime and mtime to
		//UTIME_NOW.
		if (changeboth || changeatime)
		{
			if (!timeset)
			{
				times[0].tv_nsec = UTIME_NOW;
			}
		}

		else
		{
			times[0].tv_nsec = UTIME_OMIT;
		}

		if (changeboth || changemtime)
		{
			if (!timeset)
			{
				times[1].tv_nsec = UTIME_NOW;
			}
		}

		else
		{
			times[1].tv_nsec = UTIME_OMIT;
		}

		//Actually change the time on the file,
		//at last.
		errorcode = futimens(fd, times);

		if (errorcode != 0)
		{
			fprintf(stderr, "touch: error setting time on file %s\n", argv[i]);

			errorstate = true;
		}

		//Sync the changes to the file.
		//(I don't remember exactly why I wanted
		//to sync here, but it seems safe to do so.)
		errorcode = fsync(fd);

		if (errorcode != 0)
		{
			fprintf(stderr, "touch: error syncing changes to file %s\n", argv[i]);

			errorstate = true;
		}

		//Close the file.
		errorcode = close(fd);

		if (errorcode != 0)
		{
			fprintf(stderr, "touch: error closing file %s\n", argv[i]);

			errorstate = true;
		}
	}

	//If we had an error that didn't mean we had to
	//stop processing right away, we have this check
	//to change our return status so we don't return 0.
	if (errorstate)
	{
		return 2;
	}

	//If we didn't actually touch a file, we didn't have
	//enough arguments, so throw an error.
	if (!havetouched)
	{
		fprintf(stderr, "touch: no file name argument given/found\n");
		touchusage();

		return 1;
	}

	return 0;
}

//Consider using strptime() instead?
int timeconvert(char *const string, struct timespec *const restrict result)
{
	//Holds our status in parsing
	//the date string.
	bool incentury = false;
	bool inyear = false;
	bool inmonth = false;
	bool inday = false;
	bool inhours = false;
	bool inminutes = false;
	bool inseconds = false;

	//Set to true if the string
	//was parsed 100% successfully.
	bool success = false;

	//Variables for initial sanity
	//check of input string.
	int digitsbeforeperiod = 0;
	bool hascentury = false;
	bool hasyear = false;
	bool hasseconds = false;

	//Stores the digits of each
	//section of the input string.
	int centurydigits = 0;
	int yeardigits = 0;
	int monthdigits = 0;
	int daydigits = 0;
	int hourdigits = 0;
	int minutedigits = 0;
	int seconddigits = 0;

	//Stores the parts of the
	//date that will be converted
	//to a timespec using the
	//createtimespec() function.
	int century = 0;
	int year = 0;
	int month = 0;
	int day = 0;
	int hours = 0;
	int minutes = 0;
	int seconds = 0;

	//Holds the current time in
	//case we need to get the
	//current year.
	time_t currenttime = (time_t)(-1);
	struct tm currenttimetm;

	//Increment variable.
	int i = 0;

	//Check that the option argument
	//is at least basically sane by
	//first counting the digits
	//before the period and the
	//digits in the seconds after
	//the period.
	for (i = 0; string[i] != '\0'; ++i)
	{
		if (string[i] == '.')
		{
			//There can only be one period.
			if (hasseconds)
			{
				fprintf(stderr, "touch: too many periods \".\" in -t option argument %s\n", string);

				return 10;
			}

			hasseconds = true;

			continue;
		}

		//If it's not a period or a digit, just fail.
		if ((chartonum(string[i]) < 0) || (chartonum(string[i]) > 9))
		{
			fprintf(stderr, "touch: invalid character %c encountered in -t option argument %s\n", string[i], string);

			return 5;
		}

		if (hasseconds)
		{
			++seconddigits;
		}

		else
		{
			++digitsbeforeperiod;
		}
	}

	//The string can only have 8, 10,
	//or 12 digits before the period,
	//and if there's a period, there
	//must be exactly two digits
	//following it.
	if (digitsbeforeperiod == 12)
	{
		hascentury = true;
		hasyear = true;

		incentury = true;
	}

	else if (digitsbeforeperiod == 10)
	{
		hascentury = false;
		hasyear = true;

		inyear = true;
	}

	else if (digitsbeforeperiod == 8)
	{
		hascentury = false;
		hasyear = false;

		inmonth = true;
	}

	else
	{
		fprintf(stderr, "touch: -t option argument %s not formatted correctly; too many digits before \".\" character\n", string);

		return 2;
	}

	//There has to be exactly two digits
	//to the seconds if a period was
	//encountered.
	if (hasseconds && (seconddigits != 2))
	{
		fprintf(stderr, "touch: improper amount of seconds in -t option argument %s\n", string);

		return 4;
	}

	//If we made it here, we know we have
	//something that resembles
	//[[CC]YY]MMDDhhmm[.SS]

	//Don't forget to reset seconddigits!
	seconddigits = 0;

	//Let's actually read the string now.
	for (i = 0; string[i] != '\0'; ++i)
	{
		if (incentury)
		{
			++centurydigits;

			if (centurydigits == 1)
			{
				century += chartonum(string[i]) * 10;
			}

			else if (centurydigits == 2)
			{
				century += chartonum(string[i]);

				incentury = false;
				inyear = true;
			}

			continue;
		}

		if (inyear)
		{
			++yeardigits;

			if (yeardigits == 1)
			{
				year += chartonum(string[i]) * 10;
			}

			else if (yeardigits == 2)
			{
				year += chartonum(string[i]);

				inyear = false;
				inmonth = true;
			}

			continue;
		}

		if (inmonth)
		{
			++monthdigits;

			if (monthdigits == 1)
			{
				month += chartonum(string[i]) * 10;
			}

			else if (monthdigits == 2)
			{
				month += chartonum(string[i]);

				inmonth = false;
				inday = true;
			}

			continue;
		}

		if (inday)
		{
			++daydigits;

			if (daydigits == 1)
			{
				day += chartonum(string[i]) * 10;
			}

			else if (daydigits == 2)
			{
				day += chartonum(string[i]);

				inday = false;
				inhours = true;
			}

			continue;
		}

		if (inhours)
		{
			++hourdigits;

			if (hourdigits == 1)
			{
				hours += chartonum(string[i]) * 10;
			}

			else if (hourdigits == 2)
			{
				hours += chartonum(string[i]);

				inhours = false;
				inminutes = true;
			}

			continue;
		}

		if (inminutes)
		{
			++minutedigits;

			if (minutedigits == 1)
			{
				minutes += chartonum(string[i]) * 10;
			}

			else if (minutedigits == 2)
			{
				minutes += chartonum(string[i]);

				if (!hasseconds)
				{
					if (string[i + 1] == '\0')
					{
						success = true;

						break;
					}

					fprintf(stderr, "touch: did not successfully parse -t option argument %s\n", string);

					return 12;
				}

				inminutes = false;
				inseconds = true;
			}

			continue;
		}

		if (string[i] == '.')
		{
			continue;
		}

		if (hasseconds && inseconds)
		{
			++seconddigits;

			if (seconddigits == 1)
			{
				seconds += chartonum(string[i]) * 10;
			}

			else if (seconddigits == 2)
			{
				seconds += chartonum(string[i]);

				if (string[i + 1] == '\0')
				{
					success = true;

					break;
				}

				fprintf(stderr, "touch: error parsing -t option argument %s\n", string);

				return 13;
			}
		}
	}

	//If we didn't hit one of the explicit
	//success states, parsing has failed.
	if (!success)
	{
		fprintf(stderr, "touch: failed to successfully parse -t option argument %s\n", string);

		return 15;
	}

	//If we had a century given, the year
	//is (century * 100) + year.
	if (hascentury)
	{
		if (hasyear)
		{
			year = (century * 100) + year;
		}

		else
		{
			fprintf(stderr, "touch: internal problem with error checking for -t option argument %s\n", string);

			return 17;
		}
	}

	//If we didn't have a century given,
	//we'll use the current century or
	//the previous century depending on
	//what the two-digit year is (as
	//defined in POSIX).
	//
	//We could also get the current
	//century like we get the current
	//year below; that's certainly
	//possible.
	else if (hasyear)
	{
		if (year >= 0 && year <= 68)
		{
			year = (CURRENT_CENTURY * 100) + year;
		}

		else
		{
			year = ((CURRENT_CENTURY - 1) * 100) + year;
		}
	}

	//Otherwise, we have to get the
	//current year and set the year
	//to be that.
	else
	{
		currenttime = time(NULL);

		if (currenttime == (time_t)(-1))
		{
			fprintf(stderr, "touch: in -t codepath, unable to get current time using time()\n");

			return 25;
		}

		localtime_r(&currenttime, &currenttimetm);

		//tm.tm_year stores the year as the
		//amount of years since 1900, so
		//we're adding 1900 back to it to
		//get the year as expected by
		//createtimespec(). Technically,
		//createtimespec() just subtracts it
		//again, but whatever.
		year = currenttimetm.tm_year + 1900;
	}

	//If we didn't have seconds in the option
	//argument, set to 0. (We did at the
	//beginning of the file but let's
	//make sure.)
	if (!hasseconds)
	{
		seconds = 0;
	}

	//Let's make sure all the values we've
	//just set are sane.
	if ((month < 1) || (month > 12))
	{
		fprintf(stderr, "touch: invalid month %d in -t option argument %s\n", month, string);

		return 17;
	}

	if ((day < 1) || (day > 31))
	{
		fprintf(stderr, "touch: invalid day %d in -t option argument %s\n", day, string);

		return 19;
	}

	if ((hours < 0) || (hours > 23))
	{
		fprintf(stderr, "touch: invalid hour %d in -t option argument %s\n", hours, string);

		return 21;
	}

	if ((minutes < 0) || (minutes > 59))
	{
		fprintf(stderr, "touch: invalid minute %d in -t option argument %s\n", minutes, string);

		return 23;
	}

	//Seconds goes up to 60 to account
	//for leap seconds (POSIX compliant).
	if ((seconds < 0) || (seconds > 60))
	{
		fprintf(stderr, "touch: invalid second %d in -t option argument %s\n", seconds, string);

		return 25;
	}

	//Create the timespec that will be
	//saved to result in a helper function.
	//We assume 0 nanoseconds and local
	//time.
	return createtimespec(year, month, day, hours, minutes, seconds, 0, false, result);
}

void touchusage()
{
	fprintf(stderr, "Usage: touch [-acm] [-r reffile | -t time | -d date_time] file ...\n");
	fprintf(stderr, "    reffile - file to source timestamp from to apply to touched files\n");
	fprintf(stderr, "    time - [[CC]YY]MMDDhhmm[.SS]\n");
	fprintf(stderr, "    date_time - YYYY-MM-DD[ |T]hh:mm:SS[[.|,]frac][Z]\n");

	return;
}

