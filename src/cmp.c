//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2022 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _XOPEN_SOURCE 700

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "shared/utils.h"

int cmpfiles(FILE *foneptr, FILE *ftwoptr, char *fonename, char *ftwoname,
	     bool printalot, bool printnothing);
int cmpstdin(FILE *foneptr, char *fonename, bool printalot,
	     bool printnothing, bool stdinputfirst);
void cmpusage();
int exitcmp(FILE *foneptr, FILE *ftwoptr, char *const fonename, char *const ftwoname);
int main(int argc, char *argv[]);

int cmpfiles(FILE *foneptr, FILE *ftwoptr, char *fonename, char *ftwoname,
	     bool printalot, bool printnothing)
{
	bool hasmismatched = false;

	int c_one = EOF;
	int c_two = EOF;

	int linecount = 1;
	int bytecount = 0;

	for (;;)
	{
		++bytecount;

		c_one = fgetc(foneptr);
		c_two = fgetc(ftwoptr);

		if (ferror(foneptr))
		{
			fprintf(stderr, "cmp: error reading file %s\n", fonename);

			return 5;
		}

		if (ferror(ftwoptr))
		{
			fprintf(stderr, "cmp: error reading file %s\n", ftwoname);

			return 7;
		}

		if ((c_one == EOF) || (c_two == EOF))
		{
			if (c_one != c_two)
			{
				if (!printnothing)
				{
					if (c_one == EOF)
					{
						fprintf(stderr, "cmp: EOF on %s\n", fonename);
					}

					if (c_two == EOF)
					{
						fprintf(stderr, "cmp: EOF on %s\n", ftwoname);
					}
				}

				hasmismatched = true;

				break;
			}

			break;
		}

		if (c_one != c_two)
		{
			hasmismatched = true;

			if (!printalot && !printnothing)
			{
				printf("%s %s differ: char %d, line %d\n", fonename, ftwoname, bytecount, linecount);

				break;
			}

			if (printalot)
			{
				printf("%d %o %o\n", bytecount, c_one, c_two);
			}
		}

		if ((char)c_one == '\n')
		{
			++linecount;
		}
	}

	if (hasmismatched)
	{
		return 1;
	}

	return 0;
}

int cmpstdin(FILE *foneptr, char *fonename, bool printalot,
	     bool printnothing, bool stdinputfirst)
{
	bool hasmismatched = false;

	int c_one = EOF;
	int c_two = EOF;

	int linecount = 1;
	int bytecount = 0;

	for (;;)
	{
		++bytecount;

		c_one = fgetc(foneptr);
		c_two = getchar();

		if (ferror(foneptr))
		{
			fprintf(stderr, "cmp: error reading file %s\n", fonename);

			return 5;
		}

		if ((c_one == EOF) || (c_two == EOF))
		{
			if (c_one != c_two)
			{
				if (!printnothing)
				{
					if (c_one == EOF)
					{
						fprintf(stderr, "cmp: EOF on %s\n", fonename);
					}

					if (c_two == EOF)
					{
						fprintf(stderr, "cmp: EOF on -\n");
					}
				}

				hasmismatched = true;

				break;
			}

			break;
		}

		if (c_one != c_two)
		{
			hasmismatched = true;

			if (!printalot && !printnothing)
			{
				if (stdinputfirst)
				{
					printf("- %s differ: char %d, line %d\n", fonename, bytecount, linecount);
				}

				else
				{
					printf("%s - differ: char %d, line %d\n", fonename, bytecount, linecount);
				}

				break;
			}

			if (printalot)
			{
				printf("%d %o %o\n", bytecount, c_one, c_two);
			}

			if (printnothing)
			{
				break;
			}
		}

		if ((char)c_one == '\n')
		{
			++linecount;
		}
	}

	if (hasmismatched)
	{
		return 1;
	}

	return 0;
}

void cmpusage()
{
	fprintf(stderr, "Usage: cmp [-l|-s] file1 file2\n");

	return;
}

int exitcmp(FILE *foneptr, FILE *ftwoptr, char *const fonename, char *const ftwoname)
{
	if (foneptr != NULL)
	{
		fclose(foneptr);
	}

	if (ftwoptr != NULL)
	{
		fclose(ftwoptr);
	}

	if (fonename != NULL)
	{
		free(fonename);
	}

	if (ftwoname != NULL)
	{
		free(ftwoname);
	}

	return 0;
}

int main(int argc, char *argv[])
{
	FILE *foneptr = NULL;
	FILE *ftwoptr = NULL;

	char *fonename = NULL;
	char *ftwoname = NULL;

	int errorcode = 0;

	bool printalot = false;
	bool printnothing = false;

	bool lookingforoptions = true;

	bool standardinput = false;
	bool stdinputfirst = false;

	int i = 1;
	int j = 1;

	for (i = 1; i < argc; ++i)
	{
		if (argv[i][0] != '-')
		{
			lookingforoptions = false;
		}

		else if (stringcompare(argv[i], "-"))
		{
			if (standardinput)
			{
				fprintf(stderr, "cmp: encountered two dash - arguments; behavior is undefined, exiting\n");

				return 5;
			}

			if (foneptr == NULL)
			{
				stdinputfirst = true;
			}

			standardinput = true;

			continue;
		}

		else if (lookingforoptions)
		{
			if (stringcompare(argv[i], "--"))
			{
				lookingforoptions = false;

				continue;
			}

			for (j = 1; argv[i][j] != '\0'; ++j)
			{
				if (argv[i][j] == 'l')
				{
					printalot = true;
				}

				else if (argv[i][j] == 's')
				{
					printnothing = true;
				}

				else
				{
					fprintf(stderr, "cmp: unknown option %c encountered in option string %s\n", argv[i][j], argv[i]);
					cmpusage();

					return 7;
				}
			}

			if (printalot && printnothing)
			{
				fprintf(stderr, "cmp: both -l and -s options encountered; behavior undefined, exiting\n");
				cmpusage();

				return 8;
			}

			continue;
		}

		if (foneptr == NULL)
		{
			for (j = 0; argv[i][j] != '\0'; ++j)
			{
				//Do nothing.
			}

			fonename = (char*)calloc(j + 1, sizeof(char));

			if (fonename == NULL)
			{
				fprintf(stderr, "cmp: error creating buffer for name of file 1; exiting");

				return 19;
			}

			for (j = 0; argv[i][j] != '\0'; ++j)
			{
				fonename[j] = argv[i][j];
			}

			fonename[j] = '\0';

			foneptr = fopen(fonename, "rb");

			if (foneptr == NULL)
			{
				fprintf(stderr, "cmp: could not read input file %s, exiting\n", fonename);

				exitcmp(foneptr, ftwoptr, fonename, ftwoname);

				return 9;
			}

			continue;
		}

		if (!standardinput)
		{
			for (j = 0; argv[i][j] != '\0'; ++j)
			{
				//Do nothing.
			}

			ftwoname = (char*)calloc(j + 1, sizeof(char));

			if (ftwoname == NULL)
			{
				fprintf(stderr, "cmp: error creating buffer for name of file 2; exiting");

				exitcmp(foneptr, ftwoptr, fonename, ftwoname);

				return 17;
			}

			for (j = 0; argv[i][j] != '\0'; ++j)
			{
				ftwoname[j] = argv[i][j];
			}

			ftwoname[j] = '\0';

			ftwoptr = fopen(ftwoname, "rb");

			if (ftwoptr == NULL)
			{
				fprintf(stderr, "cmp: could not read input file %s, exiting\n", ftwoname);

				exitcmp(foneptr, ftwoptr, fonename, ftwoname);

				return 9;
			}

			continue;
		}

		fprintf(stderr, "cmp: too many arguments; exiting\n");

		exitcmp(foneptr, ftwoptr, fonename, ftwoname);

		return 10;
	}

	if ((foneptr == NULL) || ((ftwoptr == NULL) && !standardinput))
	{
		fprintf(stderr, "cmp: not enough arguments; exiting\n");

		exitcmp(foneptr, ftwoptr, fonename, ftwoname);

		return 11;
	}

	if (standardinput)
	{
		errorcode = cmpstdin(foneptr, fonename, printalot,
				     printnothing, stdinputfirst);

		if ((errorcode != 0) && (errorcode != 1))
		{
			fprintf(stderr, "cmp: error comparing file %s to standard input\n", fonename);

			exitcmp(foneptr, ftwoptr, fonename, ftwoname);

			return 13;
		}

		exitcmp(foneptr, ftwoptr, fonename, ftwoname);

		return errorcode;
	}

	errorcode = cmpfiles(foneptr, ftwoptr, fonename,
			     ftwoname, printalot, printnothing);

	if ((errorcode != 0) && (errorcode != 1))
	{
		fprintf(stderr, "cmp: error comparing files %s and %s\n", fonename, ftwoname);

		exitcmp(foneptr, ftwoptr, fonename, ftwoname);

		return 15;
	}

	exitcmp(foneptr, ftwoptr, fonename, ftwoname);

	return errorcode;
}

