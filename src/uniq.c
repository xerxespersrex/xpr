//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2022, 2023 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _XOPEN_SOURCE 700

#define CHUNKSIZE 512

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "shared/utils.h"

bool isblankchar(char c);
int main(int argc, char *argv[]);
int uniq(char *inputfile, char *outputfile, bool countlines, bool justrepeats, bool norepeats, int skipfields, int skipchars);
bool uniqcompare(char *stringone, char *stringtwo, int skipfields, int skipchars);

bool isblankchar(char c)
{
	switch (c)
	{
		case ' ':
			return true;
	}

	return false;
}

int main(int argc, char *argv[])
{
	bool lookingforoptions = true;
	bool lastoption = false;

	bool lookingforfields = false;
	bool lookingforcharacters = false;

	int skipfields = 0;
	int skipchars = 0;

	bool standardinput = false;

	bool countlines = false;
	bool justrepeats = false;
	bool norepeats = false;

	char *inputfile = NULL;
	char *outputfile = NULL;

	int errorcode = 0;

	int temp = 0;

	int i = 1;
	int j = 1;

	for (i = 1; i < argc; ++i)
	{
		if (lookingforfields || lookingforcharacters)
		{
			if (lookingforfields && lookingforcharacters)
			{
				fprintf(stderr, "uniq: error: looking for both fields and characters in the same argument\n");

				return 1;
			}

			temp = stringtonum(argv[i]);

			if (temp < 0)
			{
				fprintf(stderr, "uniq: expected a number argument, received %s\n", argv[i]);

				return 1;
			}

			if (lookingforfields)
			{
				skipfields = temp;

				lookingforfields = false;
			}

			if (lookingforcharacters)
			{
				skipchars = temp;

				lookingforcharacters = false;
			}

			continue;
		}

		if (argv[i][0] != '-' && argv[i][0] != '+')
		{
			lookingforoptions = false;
		}

		else if (stringcompare(argv[i], "-"))
		{
			standardinput = true;

			lookingforoptions = false;

			continue;
		}

		else if (lookingforoptions)
		{
			if (stringcompare(argv[i], "--"))
			{
				lookingforoptions = false;

				continue;
			}

			for (j = 1; argv[i][j] != '\0'; ++j)
			{
				if (lastoption)
				{
					fprintf(stderr, "uniq: after %c option came another option in the same argument; options requiring an additional argument need to come last\n", argv[i][j - 1]);

					return 1;
				}

				if (argv[i][j] == 'c')
				{
					countlines = true;
				}

				else if (argv[i][j] == 'd')
				{
					justrepeats = true;
				}

				else if (argv[i][j] == 'u')
				{
					norepeats = true;
				}

				else if (argv[i][j] == 'f')
				{
					lookingforfields = true;
					lastoption = true;
				}

				else if (argv[i][j] == 's')
				{
					lookingforcharacters = true;
					lastoption = true;
				}

				else
				{
					fprintf(stderr, "uniq: unknown option %c encountered in option string %s\n", argv[i][j], argv[i]);

					return 1;
				}
			}

			lastoption = false;

			continue;
		}

		if (inputfile == NULL && (!standardinput))
		{
			inputfile = argv[i];
		}

		else if (outputfile == NULL)
		{
			outputfile = argv[i];
		}

		else
		{
			fprintf(stderr, "uniq: too many arguments: %s\n", argv[i]);

			return 1;
		}
	}

	if (lookingforoptions || lookingforcharacters || lookingforfields)
	{
		fprintf(stderr, "uniq: option error; still looking for options/fields/characters number\n");
		return 1;
	}

	if ((outputfile != NULL) && (inputfile != NULL) && stringcompare(inputfile, outputfile))
	{
		fprintf(stderr, "uniq: output error; outputting to same file as input has unspecified behavior\n");
		return 1;
	}

	errorcode = uniq(inputfile, outputfile, countlines, justrepeats, norepeats, skipfields, skipchars);

	if (errorcode != 0)
	{
		return 1;
	}

	return 0;
}

int uniq(char *inputfile, char *outputfile, bool countlines, bool justrepeats, bool norepeats, int skipfields, int skipchars)
{
	char *testline = NULL;
	char *currentline = NULL;

	int count = 0;

	FILE *finptr = NULL;
	FILE *foutptr = NULL;

	int chunks = 0;

	int bytecount = 0;

	bool standardinput = false;
	bool writetofile = false;

	int c = 0;

	if (inputfile == NULL)
	{
		standardinput = true;

		clearerr(stdin);
	}

	if (outputfile != NULL)
	{
		writetofile = true;
	}

	if (!standardinput)
	{
		finptr = fopen(inputfile, "rb");

		if (finptr == NULL)
		{
			fprintf(stderr, "uniq: error opening file %s to read\n", inputfile);

			return -10;
		}
	}

	if (writetofile)
	{
		foutptr = fopen(outputfile, "wb");

		if (foutptr == NULL)
		{
			fprintf(stderr, "uniq: error opening file %s to write\n", outputfile);

			if (finptr != NULL)
			{
				fclose(finptr);

				finptr = NULL;
			}

			return -34;
		}
	}

	for (;;)
	{
		if (standardinput)
		{
			c = getchar();
		}

		else
		{
			c = fgetc(finptr);

			if (ferror(finptr))
			{
				if (currentline != NULL)
				{
					free(currentline);

					currentline = NULL;
				}

				if (testline != NULL)
				{
					free(testline);

					testline = NULL;
				}

				fclose(finptr);

				finptr = NULL;

				if (foutptr != NULL)
				{
					fclose(foutptr);

					foutptr = NULL;
				}

				fprintf(stderr, "uniq: error reading file %s\n", inputfile);

				return -15;
			}
		}

		if (c == EOF)
		{
			if (finptr != NULL)
			{
				fclose(finptr);

				finptr = NULL;
			}

			break;
		}

		if ((bytecount + 1) >= (CHUNKSIZE * chunks))
		{
			++chunks;

			currentline = (char*)realloc(currentline, CHUNKSIZE * chunks * sizeof(char));
		}

		currentline[bytecount] = (char)(c);
		currentline[bytecount + 1] = '\0';

		++bytecount;

		if ((char)(c) == '\n')
		{
			if (testline == NULL)
			{
				testline = currentline;

				count = 1;
			}

			else if (uniqcompare(testline, currentline, skipfields, skipchars))
			{
				free(currentline);

				++count;
			}

			else
			{
				if (((count > 1) && justrepeats) ||
				    ((count == 1) && norepeats) ||
				    (!justrepeats && !norepeats))
				{
					if (countlines)
					{
						(writetofile) ? fprintf(foutptr, "%d ", count) : printf("%d ", count);
					}

					(writetofile) ? fprintf(foutptr, "%s", testline) : printf("%s", testline);
				}

				free(testline);

				testline = currentline;

				count = 1;
			}

			currentline = NULL;

			chunks = 0;
			bytecount = 0;
		}
	}

	if (currentline != NULL)
	{
		if (testline != NULL)
		{
			//This means we ended on a final line that wasn't
			//terminated by a newline.
			if (uniqcompare(testline, currentline, skipfields, skipchars))
			{
				++count;

				if (((count > 1) && justrepeats) ||
				    ((count == 1) && norepeats) ||
				    (!justrepeats && !norepeats))
				{
					if (countlines)
					{
						(writetofile) ? fprintf(foutptr, "%d ", count) : printf("%d ", count);
					}

					(writetofile) ? fprintf(foutptr, "%s", testline) : printf("%s", testline);
				}
			}

			else
			{
				if (((count > 1) && justrepeats) ||
				    ((count == 1) && norepeats) ||
				    (!justrepeats && !norepeats))
				{
					if (countlines)
					{
						(writetofile) ? fprintf(foutptr, "%d ", count) : printf("%d ", count);
					}

					(writetofile) ? fprintf(foutptr, "%s", testline) : printf("%s", testline);
				}

				if (!justrepeats)
				{

					if (countlines)
					{
						(writetofile) ? fprintf(foutptr, "1 ") : printf("1 ");
					}

					(writetofile) ? fprintf(foutptr, "%s", currentline) : printf("%s", currentline);
				}
			}

			free(testline);
			free(currentline);

			testline = NULL;
			currentline = NULL;
		}

		else
		{
			if (!justrepeats)
			{

				if (countlines)
				{
					(writetofile) ? fprintf(foutptr, "1 ") : printf("1 ");
				}

				(writetofile) ? fprintf(foutptr, "%s", currentline) : printf("%s", currentline);
			}

			free(currentline);

			currentline = NULL;
		}
	}

	else
	{
		if (testline != NULL)
		{
			if (((count > 1) && justrepeats) ||
			    ((count == 1) && norepeats) ||
			    (!justrepeats && !norepeats))
			{
				if (countlines)
				{
					(writetofile) ? fprintf(foutptr, "%d ", count) : printf("%d ", count);
				}

				(writetofile) ? fprintf(foutptr, "%s", testline) : printf("%s", testline);
			}
		}
	}

	if (foutptr != NULL)
	{
		fclose(foutptr);

		foutptr = NULL;
	}

	return 0;
}

bool uniqcompare(char *stringone, char *stringtwo, int skipfields, int skipchars)
{
	int stringoneloc = 0;
	int stringtwoloc = 0;

	int fieldscounted = 0;
	bool infield = false;

	int charsskipped = 0;

	int i = 0;
	int j = 0;

	bool status = false;

	char* stringonecpy = NULL;
	char* stringtwocpy = NULL;

	if ((stringone == NULL) || (stringtwo == NULL))
	{
		return false;
	}

	stringonecpy = stringcopy(stringone);
	stringtwocpy = stringcopy(stringtwo);

	if ((stringonecpy == NULL) || (stringtwocpy == NULL))
	{
		return false;
	}

	//Remove trailing newlines from the strings;
	//per the standard, trailing newlines
	//should not be considered for the comparison.
	while (stringonecpy[i] != '\0')
	{
		++i;
	}

	while ((i > 0) && (stringonecpy[i - 1] == '\n'))
	{
		stringonecpy[i - 1] = '\0';
		--i;
	}

	i = 0;

	while (stringtwocpy[i] != '\0')
	{
		++i;
	}

	while ((i > 0) && (stringtwocpy[i - 1] == '\n'))
	{
		stringtwocpy[i - 1] = '\0';
		--i;
	}

	if ((skipfields == 0) && (skipchars == 0))
	{
		status = stringcompare(stringonecpy, stringtwocpy);

		free(stringonecpy);
		free(stringtwocpy);

		return status;
	}

	if (skipfields > 0)
	{
		infield = false;
		fieldscounted = 0;

		for (i = stringoneloc; stringonecpy[i] != '\0'; ++i)
		{
			if ((infield == false) && (!isblankchar(stringonecpy[i])))
			{
				infield = true;
			}

			else if ((infield == true) && (isblankchar(stringonecpy[i])))
			{
				infield = false;
				++fieldscounted;

				if (fieldscounted >= skipfields)
				{
					break;
				}
			}
		}

		stringoneloc = i;

		infield = false;
		fieldscounted = 0;

		for (i = stringtwoloc; stringtwocpy[i] != '\0'; ++i)
		{
			if ((infield == false) && (!isblankchar(stringtwocpy[i])))
			{
				infield = true;
			}

			else if ((infield == true) && (isblankchar(stringtwocpy[i])))
			{
				infield = false;
				++fieldscounted;

				if (fieldscounted >= skipfields)
				{
					break;
				}
			}
		}

		stringtwoloc = i;
	}

	if (skipchars > 0)
	{
		charsskipped = 0;

		for (i = stringoneloc; (stringonecpy[i] != '\0') && (charsskipped < skipchars); ++i)
		{
			++charsskipped;
		}

		stringoneloc = i;

		charsskipped = 0;

		for (i = stringtwoloc; (stringtwocpy[i] != '\0') && (charsskipped < skipchars); ++i)
		{
			++charsskipped;
		}

		stringtwoloc = i;
	}

	i = stringoneloc;
	j = stringtwoloc;

	while ((stringonecpy[i] != '\0') && (stringtwocpy[j] != '\0') && (stringonecpy[i] == stringtwocpy[j]))
	{
		++i;
		++j;
	}

	if (stringonecpy[i] != stringtwocpy[j])
	{
		status = false;
	}

	else
	{
		status =  true;
	}

	free(stringonecpy);
	free(stringtwocpy);

	return status;
}

