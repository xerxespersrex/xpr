//    xpr - A rewrite of the SUS core programs.
//    Copyright (C) 2022 Ethan Masse
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _XOPEN_SOURCE 700

//POSIX gives the minimum amount
//of output files for tee as 13.
#define MAX_FILES 13

#include <signal.h>
#include <stdbool.h>
#include <stdio.h>

#include "shared/utils.h"

int main(int argc, char *argv[])
{
	FILE *files[MAX_FILES];
	int c = EOF;
	int i = 1;
	int j = 0;
	int filecounter = 0;
	int errorcode = 0;

	bool sigintignored = false;

	bool lookingforoptions = true;

	bool append = false;

	for (i = 0; i < MAX_FILES; ++i)
	{
		files[i] = NULL;
	}

	//Find first argument that's a file.
	for (i = 1; i < argc; ++i)
	{
		if (argv[i][0] != '-')
		{
			lookingforoptions = false;
		}

		//tee does not take "-" to mean stdin,
		//it specifically must read it as the
		//file named "-".
		else if (stringcompare(argv[i], "-"))
		{
			lookingforoptions = false;
		}

		else if (lookingforoptions)
		{
			if (stringcompare(argv[i], "--"))
			{
				lookingforoptions = false;

				continue;
			}

			for (j = 1; argv[i][j] != '\0'; ++j)
			{
				//-a: append to file(s) instead
				//of overwriting.
				if (argv[i][j] == 'a')
				{
					append = true;
				}

				//-i: ignore SIGINT signal
				else if (argv[i][j] == 'i')
				{
					if (!sigintignored)
					{
						//Set SIGINT to ignore and catch
						//any errors.
						if (signal(SIGINT, SIG_IGN) == SIG_ERR)
						{
							fprintf(stderr, "tee: error with -i option; unable to disable SIGINT\n");

							return 30;
						}

						sigintignored = true;
					}
				}
			}

			continue;
		}

		if (filecounter >= MAX_FILES)
		{
			fprintf(stderr, "tee: only supports %d files, ignoring %s\n", MAX_FILES, argv[i]);

			continue;
		}

		//Open file to append.
		if (append)
		{
			files[filecounter] = fopen(argv[i], "ab");
		}

		//Open file destructively, replacing contents.
		else
		{
			files[filecounter] = fopen(argv[i], "wb");
		}

		if (files[filecounter] == NULL)
		{
			if (append)
			{
				fprintf(stderr, "tee: error opening file %s to append, skipping\n", argv[i]);
			}

			else
			{
				fprintf(stderr, "tee: error opening file %s to overwrite, skipping\n", argv[i]);
			}

			continue;
		}

		++filecounter;
	}

	for (;;)
	{
		c = getchar();

		if (c == EOF)
		{
			break;
		}

		printf("%c", (char)c);

		for (i = 0; i < filecounter; ++i)
		{
			if (files[i] == NULL)
			{
				continue;
			}

			errorcode = fputc(c, files[i]);

			if ((errorcode == EOF) || ferror(files[i]))
			{
				fprintf(stderr, "tee: error writing to file number %d, skipping from now on\n", i);

				fclose(files[i]);

				files[i] = NULL;
			}
		}
	}

	for (i = 0; i < filecounter; ++i)
	{
		if (files[i] != NULL)
		{
			fclose(files[i]);

			files[i] = NULL;
		}
	}

	return 0;
}

