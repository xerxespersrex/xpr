#!/bin/sh

#    xpr - A rewrite of the SUS core programs.
#    Copyright (C) 2022, 2023 Ethan Masse
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#=========== setup ===========

#Sets GNU utilities and programs to follow POSIX
#conventions more closely. Gives a more
#standards compliant base system to compare to
#if the testing system is running GNU coreutils.
export POSIXLY_CORRECT=true

echo "======================"
echo "Starting xpr tests"
echo "======================"

echo
echo "Making .testing/ directory..."

if [ ! -d ".testing" ]; then
	mkdir .testing
else
	echo ".testing/ directory already exists; please manually delete it and rerun this script"
	exit 1
fi

if [ ! -d ".testing" ]; then
	echo "Unable to create .testing/ directory; exiting"
	exit 1
fi

#=========== basename ===========

echo
echo "Testing basename..."

if [ ! -e bin/basename ]; then
	echo "[note] basename not found in bin/ directory, skipping"
else

	if [ x"$(bin/basename "")" != x"." ]; then
		echo "[FAIL] failed \"\" test"
	else
		echo "[PASS] passed \"\" test"
	fi

	if [ x"$(bin/basename /home/xerxes)" != x"xerxes" ]; then
		echo "[FAIL] failed /home/xerxes test"
	else
		echo "[PASS] passed /home/xerxes test"
	fi

	if [ x"$(bin/basename -- /home/xerxes)" != x"xerxes" ]; then
		echo "[FAIL] failed -- /home/xerxes test"
	else
		echo "[PASS] passed -- /home/xerxes test"
	fi

	if [ x"$(bin/basename /)" != x"/" ]; then
		echo "[FAIL] failed / test"
	else
		echo "[PASS] passed / test"
	fi

	if [ x"$(bin/basename /xpr)" != x"xpr" ]; then
		echo "[FAIL] failed /xpr test"
	else
		echo "[PASS] passed /xpr test"
	fi

	if [ x"$(bin/basename /xpr/)" != x"xpr" ]; then
		echo "[FAIL] failed /xpr/ test"
	else
		echo "[PASS] passed /xpr/ test"
	fi

	if [ x"$(bin/basename /src/xpr/touch.c .c)" != x"touch" ]; then
		echo "[FAIL] failed /src/xpr/touch.c .c test"
	else
		echo "[PASS] passed /src/xpr/touch.c .c test"
	fi

	if [ x"$(bin/basename /src/xpr/touch.c touch.c)" != x"touch.c" ]; then
		echo "[FAIL] failed /src/xpr/touch.c touch.c test"
	else
		echo "[PASS] passed /src/xpr/touch.c touch.c test"
	fi

	if [ x"$(basename -- -- touch.c)" != x"--" ]; then
		echo "[FAIL] failed -- -- touch.c test"
	else
		echo "[PASS] passed -- -- touch.c test"
	fi

	if [ x"$(basename //d/dd/dd/dddd/dd/ d)" != x"d" ]; then
		echo "[FAIL] failed //d/dd/dd/dddd/dd/ d test"
	else
		echo "[PASS] passed //d/dd/dd/dddd/dd/ d test"
	fi

fi

#=========== cmp ===========

echo
echo "Testing cmp..."

if [ ! -e bin/cmp ]; then
	echo "[note] cmp not found in bin/ directory, skipping"
else

	cat >.testing/._cmptest_xpr <<EOF
You enter a dark dungeon. The light sound of dripping brings you to a sense of normality despite the darkness stealing away your breath. On the wall, mounted elegantly, is a sword, its hilt engraved deeply with the following glyphs:

夫未战而庙算胜者，得算多也；未战而庙算不胜者，得算少也。多算胜少算，而况于无算乎！吾以此观之，胜负见矣。

The sword looks ancient and priceless, but the engraving is in a language that you're unable to understand.

I'm not interesting enough to come up with anything else to write to incorporate unicode, so I'm just going to dump some emojis here: 🐲⛑📟📠🉑

Time is like a drop in an ocean. Radishes.

EOF

	cp .testing/._cmptest_xpr .testing/._cmptest_xpr_copy

	cat >.testing/._cmptest2_xpr <<EOF
1
2
3
4
5
1213123
1231
231
231
21421
fasdfas
dffas
as
fdj
231412
3123
41234

EOF

	#----Same files----
	bin/cmp .testing/._cmptest_xpr .testing/._cmptest_xpr_copy &> /dev/null

	if [ x"${?}" != x"0" ]; then
		echo "[FAIL] default cmp on copied file failed"
	else
		echo "[PASS] default cmp on copied file passed"
	fi

	bin/cmp -s .testing/._cmptest_xpr .testing/._cmptest_xpr_copy &> /dev/null

	if [ x"${?}" != x"0" ]; then
		echo "[FAIL] -s cmp on copied file failed"
	else
		echo "[PASS] -s cmp on copied file passed"
	fi

	bin/cmp -l .testing/._cmptest_xpr .testing/._cmptest_xpr_copy &> /dev/null

	if [ x"${?}" != x"0" ]; then
		echo "[FAIL] -l cmp on copied file failed"
	else
		echo "[PASS] -l cmp on copied file passed"
	fi

	#----Different files----
	bin/cmp .testing/._cmptest_xpr .testing/._cmptest2_xpr &> /dev/null

	if [ x"${?}" != x"1" ]; then
		echo "[FAIL] default cmp on differing files failed"
	else
		echo "[PASS] default cmp on differing files passed"
	fi

	bin/cmp -s .testing/._cmptest_xpr .testing/._cmptest2_xpr &> /dev/null

	if [ x"${?}" != x"1" ]; then
		echo "[FAIL] -s cmp on differing files failed"
	else
		echo "[PASS] -s cmp on differing files passed"
	fi

	bin/cmp -l .testing/._cmptest_xpr .testing/._cmptest2_xpr &> /dev/null

	if [ x"${?}" != x"1" ]; then
		echo "[FAIL] -l cmp on differing files failed"
	else
		echo "[PASS] -l cmp on differing files passed"
	fi

fi

#=========== cp ===========

echo
echo "Testing cp..."

if [ ! -e bin/cp ]; then
	echo "[note] cp not found in bin/ directory, skipping"
else

	cat >.testing/._cptest_xpr <<EOF
You enter a dark dungeon. The light sound of dripping brings you to a sense of normality despite the darkness stealing away your breath. On the wall, mounted elegantly, is a sword, its hilt engraved deeply with the following glyphs:

夫未战而庙算胜者，得算多也；未战而庙算不胜者，得算少也。多算胜少算，而况于无算乎！吾以此观之，胜负见矣。

The sword looks ancient and priceless, but the engraving is in a language that you're unable to understand.

I'm not interesting enough to come up with anything else to write to incorporate unicode, so I'm just going to dump some emojis here: 🐲⛑📟📠🉑

Time is like a drop in an ocean. Radishes.

EOF

	bin/cp .testing/._cptest_xpr .testing/._cptest_copy_xpr

	cmp .testing/._cptest_xpr .testing/._cptest_copy_xpr

	if [ x"$?" != x"0" ]; then
		echo "[FAIL] cp failed cmp test"
	else
		echo "[PASS] cp passed cmp test"
	fi

	#Insert more tests as cp is more developed.
fi


#=========== dirname ===========

echo
echo "Testing dirname..."

if [ ! -e bin/dirname ]; then
	echo "[note] dirname not found in bin/ directory, skipping"
else

	if [ x"$(bin/dirname "")" != x"." ]; then
		echo "[FAIL] failed \"\" test"
	else
		echo "[PASS] passed \"\" test"
	fi

	if [ x"$(bin/dirname /home/xerxes)" != x"/home" ]; then
		echo "[FAIL] failed /home/xerxes test"
	else
		echo "[PASS] passed /home/xerxes test"
	fi

	if [ x"$(bin/dirname -- /home/xerxes)" != x"/home" ]; then
		echo "[FAIL] failed -- /home/xerxes test"
	else
		echo "[PASS] passed -- /home/xerxes test"
	fi

	if [ x"$(bin/dirname /)" != x"/" ]; then
		echo "[FAIL] failed / test"
	else
		echo "[PASS] passed / test"
	fi

	if [ x"$(bin/dirname /xpr)" != x"/" ]; then
		echo "[FAIL] failed /xpr test"
	else
		echo "[PASS] passed /xpr test"
	fi

	if [ x"$(bin/dirname /xpr/)" != x"/" ]; then
		echo "[FAIL] failed /xpr/ test"
	else
		echo "[PASS] passed /xpr/ test"
	fi

	if [ x"$(bin/dirname xpr/)" != x"." ]; then
		echo "[FAIL] failed xpr/ test"
	else
		echo "[PASS] passed xpr/ test"
	fi

	if [ x"$(bin/dirname -- --)" != x"." ]; then
		echo "[FAIL] failed -- -- test"
	else
		echo "[PASS] passed -- -- test"
	fi

	if [ x"$(bin/dirname src/xpr/touch.c)" != x"src/xpr" ]; then
		echo "[FAIL] failed src/xpr/touch.c test"
	else
		echo "[PASS] passed src/xpr/touch.c test"
	fi

	if [ x"$(bin/dirname /home/xerxes/src/xpr/touch.c)" != x"/home/xerxes/src/xpr" ]; then
		echo "[FAIL] failed /home/xerxes/src/xpr/touch.c test"
	else
		echo "[PASS] passed /home/xerxes/src/xpr/touch.c test"
	fi

	if [ x"$(bin/dirname //d/dd/dd/dddd/dd/)" != x"//d/dd/dd/dddd" ]; then
		echo "[FAIL] failed //d/dd/dd/dddd/dd/ test"
	else
		echo "[PASS] passed //d/dd/dd/dddd/dd/ test"
	fi

fi

#=========== touch ===========

echo
echo "Testing touch..."

if [ ! -e bin/touch ]; then
	echo "[note] touch not found in bin/ directory, skipping"
else
	touch .testing/._touchtest1_host
	bin/touch .testing/._touchtest1_xpr

	cmp .testing/._touchtest1_host .testing/._touchtest1_xpr

	if [ x"$?" != x"0" ]; then
		echo "[FAIL] touch failed on cmp test"
	else
		echo "[PASS] touch passed cmp test"
	fi

	bin/touch -c .testing/._touchtest2_xpr

	cat .testing/._touchtest2_xpr 2> /dev/null

	if [ x"$?" != x"1" ]; then
		echo "[FAIL] touch -c created a file"
	else
		echo "[PASS] touch -c passed"
	fi

	#We can't get a file's actual atime/mtime date on
	#POSIX besides unparseable ls without some wizardry
	#(apparently it's possible with pax and ustar),
	#or we can write a small testing program that stats
	#the file and tests it that way. That isn't written
	#yet, so we'll skip testing that for now.
	echo "[note] touch test not yet capable of testing generated file atime/mtime, skipping"

	#We also could chown a test file and see if we're
	#able to touch -a or touch -m it with write
	#permission but not ownership, but some
	#implementations (i.e. GNU/Linux) are such that
	#chown requires root. So this test would require
	#both the above program and would need to be run as
	#root.
	echo "[note] touch test not yet capable of testing edge case behavior with files with write permission but not owned by current user, skipping"
fi

#=========== cleanup ===========

echo
echo "Removing .testing/ directory..."

rm -rf .testing

exit 0

